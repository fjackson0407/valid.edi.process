﻿using Common;
using OrderReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the date of the PO to make a report");
            var StringPODate =  Console.ReadLine();
            DateTime PODate;
            if (DateTime.TryParse(StringPODate, out PODate))
            {
                Settings settings = new Settings();
                WeeklyReports cWeeklyReports = new WeeklyReports(settings);
                 cWeeklyReports.GetPOForTheWeek(PODate);

            }
        }
    }
}
