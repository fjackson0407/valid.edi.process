﻿using Common;
using EDIException;
using EDIIntake;
using EDIServiceV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VpsEDIImport
{
    class Program
    {
        static void Main(string[] args)
        {
            Settings _settings = new Settings(); 
            Console.WriteLine("This program will import an EDI 850 and make ASNs for Amex and Visa/Mastercard");
            Console.WriteLine("The data will go to the old and new database");
            Console.WriteLine($"The old database is {_settings.ConnectionStringOld } ");
            Console.WriteLine($"The new database is {_settings.ConnectionString}");
            Console.WriteLine("Please type Start to continue");
            var readLine = Console.ReadLine();
            if (readLine != null && readLine.ToLower() == "start")
            {
                StartImport(_settings);
            }

        }

        private static void StartImport(Settings _Settings )
        {
            try
            {
                
                EDIImport cEDIImport = new EDIImport(_Settings);
                cEDIImport.EDI850();
                Console.WriteLine($"Process completed at {DateTime.Now}");
                Console.ReadLine();
            }
            catch (ExceptionsEDI ex )
            {
                Console.WriteLine($"Error {ex}");
            }
          
        }
    }
}
