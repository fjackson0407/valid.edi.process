﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Common;
using EDIException;
using static Helpers.EDIHelperFunctions;
using EDIIntake;
using EDIServiceV1;
using VaildUSAASNBuild;
using ASNService;

namespace EDIServiceV1
{
    public class EDIImport
    {
        Settings m_settings = new Settings();

        public EDIImport(Settings _settings)
        {
            m_settings = _settings;
        }

        /// <summary>
        /// Entry point to import EDI 850 file
        /// </summary>
        public void EDI850()
        {
            if (!Directory.Exists(m_settings.EDI850FolderPath))
            { throw new ExceptionsEDI($"{Help} {ErrorCodes.HSAError3} {m_settings.EDI850FolderPath }"); }
            ProcessDirectory();
        }

        private void ProcessDirectory()
        {
            string[] fileEntries = Directory.GetFiles(m_settings.EDI850FolderPath);
            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName);
            }
            BuildASnForStores();

        }

        private void BuildASnForStores()
        {
            ASNMaker cASNMakerV2 = new ASNMaker(m_settings);
            cASNMakerV2.BuildASNForAllDC();
            ASNBuild cASNBuildV1 = new ASNBuild(m_settings);
            cASNBuildV1.ProcessASN();
        }

        /// <summary>
        /// Take each file and import it into the old and new database 
        /// </summary>
        /// <param name="cFileName"></param>
        private void ProcessFile(string cFileName)
        {
            
            EDI850ImportV2 cNewEDI850 = new EDI850ImportV2(m_settings , cFileName );
            cNewEDI850.ParseFile();
            EDIPOServiceV1 cEDIPOServiceV1 = new EDIPOServiceV1(m_settings, cFileName);
            cEDIPOServiceV1.ParseEDI850();
            
        }
    }
}
