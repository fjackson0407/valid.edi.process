﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    [Table("CartonDetail856")]
    public class CartonInfoDTO
    {

        public CartonInfoDTO()
        {
            
        }
        [Key]
        public Guid ID { get; set; }
        
        [NotMapped]
        public int BundleCountMin
        {

            get
            {
                return CardCountMin / 25;
            }

        }

        [NotMapped]
        public int BundleCountMax
        {
            get
            {
                return CardCountMax / 25;
            }
        }
        public int CardCountMin { get; set; }
        public int CardCountMax { get; set; }
        public string Size { get; set; }
        public string SKU { get; set; }
        public string Author { get; set; }
        public DateTime DTS { get; set; }
      

        
    }
}
