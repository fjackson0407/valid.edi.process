﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    [Table("ShipFromInformation")]
  public  class ShipFromInformationDTO
    {
        public Guid Id { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string DUNSOrLocationNumber { get; set; }
        public string BillAndShipToCodes { get; set; }
    }
}
