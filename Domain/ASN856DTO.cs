﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    [Table("ASNFile856")]
  public  class ASNFile856DTO
    {
        public Guid Id { get; set; }
        public string File { get; set; }
        public DateTime DTS { get; set; }
        public Guid? StoreFK { get; set; }
        public virtual Order850DTO Store { get; set; }


    }
}
