﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("OrderDetail850")]
    public  class OrderDetail850DTO
    {
        
        public Guid Id { get; set; }
        public string DPCI { get; set; }
        public int QtyOrdered { get; set; }
        public int CustomerLineNumber { get; set; }
        public string ItemDescription { get; set; }
        public string UPC { get; set; }
        
        
        [ForeignKey("Order850")]
        public Guid? Order850_FK { get; set; }
        public virtual Order850DTO Order850 { get; set; }


        [ForeignKey("PackInfo")]
        public Guid? PackInfo_FK { get; set; }
        public virtual PackInfoDTO PackInfo { get; set; }

        
        public string VendorNumber { get; set; }
        public int  ItemStatus { get; set; }


    }
}
