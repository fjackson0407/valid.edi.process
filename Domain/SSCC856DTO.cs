﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    [Table("SSCC856")]
   public  class SSCC856DTO
    {
        [Key]
        public Guid Id { get; set; }
        public int SequenceNumber { get; set; }
        public int Used { get; set; }
        public DateTime DTS { get; set; }
        [NotMapped]
        public string  BarCode { get; set; }
    }
}
