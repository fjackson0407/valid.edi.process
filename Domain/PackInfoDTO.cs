﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("PackInfo")]
    public  class PackInfoDTO
    {
        [Key]
        public Guid Id { get; set; }
        public string  CompanyCode  { get; set; }
        public float WeightOfPack  { get; set; }
    }
}
