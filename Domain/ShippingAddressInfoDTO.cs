﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("ShippingAddressInfo")]
    public class ShippingAddressInfoDTO
    {
        [Key]
        public Guid Id { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string BillAndShipToCodes { get; set; }
        public string StoreID { get; set; }
        
    }
}
