﻿using System;
using System.Collections.Generic;

namespace Domain
{
 public   class ASNOrderInfo856DTO
    {
        public ASNOrderInfo856DTO()
        {
            ASNStoreDetail = new List<Domain.OrderDetail850DTO>();
            Cartons = new List<Domain.Carton856DTO>();
        }

        public Guid Id { get; set; }
        public string StoreNumber { get; set; }
        public string DCNumber { get; set; }
        public double PkgWeight { get; set; }
        public List<OrderDetail850DTO> ASNStoreDetail { get; set; }

        public List<Carton856DTO> Cartons { get; set;  }
    }
}
