﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    [Table("SerialRange850")]
    public   class SerialRange850DTO
    {

        public Guid ID { get; set; }
        public Int64 SerialNumber { get; set; }
        public string Serialbundle { get; set; }
        public virtual ASNOrderDetailDTO856 ASNOrderDetailDTO856 { get; set; }
        public Guid? ASNOrderDetailFK { get; set;  }

    }
}
