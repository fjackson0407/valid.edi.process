﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
 public   class Carton
    {
        public Guid? Id { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public string Size  { get; set; }
        public string SKU { get; set;  }
        public string Author { get; set; }
        public string  PONumber { get; set; }
        public int BoxCount { get; set; }
        public String  CardType { get; set; }
    }

    public class BOLSummary
    {
        public string  PONumber  { get; set; }
        public int BoxCount  { get; set; }
        public int Weight { get; set; }
        public string  CardType { get; set; }
    }
}
