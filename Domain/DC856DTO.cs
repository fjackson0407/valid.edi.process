﻿using Common;
using System;
using System.Collections.Generic;

namespace Domain
{
    public class DC856
    {
        public Guid ID { get; set; }
        public string DcNumber { get; set; }
        public string StoreNumber { get; set; }
        public string CompanyCode { get; set; }
        public string CustomerNumber { get; set; }
        public DateTime PODate { get; set; }
        public string PONumber { get; set; }
        public string PurchaseOrderSourceID { get; set; }
        public string VendorNumber { get; set; }
        
        public string ShipmentID
        {
            get
            {
                return
                    ValidConstants.ValidName + CommonFunctions.RandomString(ValidConstants.shiplength);
            }
        }
        public IEnumerable<Order850DTO> Stores850 { get; set; }
        public virtual ShippingAddressInfoDTO AddressInfo { get; set; }
        
    }
}
