﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    [Table("Order850")]
    public class Order850DTO
    {
        public Order850DTO()
        {

            ASN = new HashSet<ASNFile856DTO>();
            
            BOL = new HashSet<BOL856DTO>();
            
            StoreItemDetail = new HashSet<OrderDetailDTO850>();
        }
        [Key]
        #region Class members 
        public Guid Id { get; set; }
         public DateTime DTS { get; set; }

        public string CompanyCode { get; set; }

        public string VendorNumber { get; set; }
        public string CustomerNumber { get; set; }

        public string PONumber { get; set; }

        [NotMapped]
        public string POSubstring
        {
            get
            {
                string[] parts = PONumber.Split('-');
                return string.Format("{0}-{1}", parts[0], parts[1]);
            }

        }

        public virtual string Label { get; set; }
        public string OrderStoreNumber { get; set; }
        public DateTime PODate { get; set; }

        public string ShipDate { get; set; }

        public string CancelDate { get; set; }

        [Column( Order = 3, TypeName = "varchar")]
        public string DCNumber { get; set; }

        public int PickStatus { get; set; }
        public string BillToAddress { get; set; }


        public string DocumentId { get; set; }

        public int ASNStatus { get; set; }

        public Guid? ASNFileOutBoundFK { get; set; }


        public string User { get; set; }
        public bool InUse { get; set; }
        public DateTime? OrderCompleteTime { get; set; }

        public virtual ICollection<ASNFile856DTO> ASN { get; set; }
        
        
        public virtual ICollection<BOL856DTO> BOL { get; set; }
        public virtual  ICollection<OrderDetailDTO850> StoreItemDetail { get; set;  }

        #endregion
    }
}
