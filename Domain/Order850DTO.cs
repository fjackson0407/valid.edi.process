﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("Order850")]
    public class Order850DTO
    {
        public Order850DTO()
        {
            BOL = new HashSet<BOL856DTO>();
            OrderDetail = new HashSet<OrderDetail850DTO>();
            ASNOrderDetail = new HashSet<ASNOrderDetail856DTO>();
            Cartons = new HashSet<Carton856DTO>();
        }

        public virtual ICollection<BOL856DTO> BOL { get; set; }
        public virtual ICollection<OrderDetail850DTO> OrderDetail { get; set; }
        public virtual ICollection<ASNOrderDetail856DTO> ASNOrderDetail { get; set; }
        /// <summary>
        /// NOTE Jesse please do not change this to not mapped it will break my code thanks. Cisco  3/16/2017
        /// </summary>
        public virtual ICollection<Carton856DTO> Cartons { get; set; }

        [Key]
        public Guid Id { get; set; }
        public DateTime DTS { get; set; }
        public string CompanyCode { get; set; }
        public string VendorNumber { get; set; }
        public string CustomerNumber { get; set; }
        public string PONumber { get; set; }
        public string OrderStoreNumber { get; set; }
        public DateTime PODate { get; set; }
        public string ShipDate { get; set; }
        public string DCNumber { get; set; }
        public int ASNStatus { get; set; }
        public int PickStatus { get; set; }
        public string PurchaseOrderSourceID { get; set; }
        public string DocumentId { get; set; }
        public string InUseBy { get; set; }
        [ForeignKey("AddressInfo")]
        public Guid? AddressInfo_FK { get; set; }
        public virtual ShippingAddressInfoDTO AddressInfo { get; set; }

        public Guid? ASNFile856DTO_FK { get; set; }
        public virtual ASNFile856DTO ASNFile856DTO { get; set; }

    }
        
}
