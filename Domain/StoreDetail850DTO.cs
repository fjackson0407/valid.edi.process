﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
   public  class OrderDetail850DTO
    {
        public OrderDetail850DTO()
        {
            
        }
        public Guid Id { get; set; }
        public string DPCI { get; set; }
        public string OrderStoreNumber { get; set; }
        public int QtyOrdered { get; set; }
        public int CustomerLineNumber { get; set; }
        public PackWeight850DTO PkgWeight { get; set; }
        public string ItemDescription { get; set; }
        public string UPC { get; set; }
        public string   VN  { get; set; }

    }
}
