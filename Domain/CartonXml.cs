﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
   public  class CartonXml
    {
        public Guid? ID { get; set; }
        public string PurchaseOrderSourceID { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string StoreNumber { get; set; }
        public DateTime  PurchaseOrderDate { get; set; }
        public string  BarCode  { get; set; }
        public int QTYOfCarton { get; set; }
        public int NumbereofCartons { get; set; }
        public ICollection<ASNOrderDetail856DTO> CartonItems  { get; set; }
    }
}
