﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("ASNFile856")]
  public  class ASNFile856DTO
    {
        public ASNFile856DTO()
        {
            Order = new HashSet<Order850DTO>();
        }
            
        public Guid Id { get; set; }
        public string File { get; set; }
        public DateTime DTS { get; set; }

        public virtual ICollection<Order850DTO> Order { get; set; }
    }
}
