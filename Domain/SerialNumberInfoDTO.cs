﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("SerialNumberInfo")]
    public   class SerialNumberInfoDTO
    {
        public Guid ID { get; set; }
        public Int64 SerialNumber { get; set; }
        public string Serialbundle { get; set; }
        public Guid? ASNOrderDetail856_FK { get; set; }
        public virtual ASNOrderDetail856DTO ASNOrderDetail856 { get; set; }


    }
}
