﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("Carton856")]

    public class Carton856DTO
    {
        public Carton856DTO()
        {
            ASNStoreDetail = new HashSet<ASNOrderDetail856DTO>();
        }
        
        [Key]
        public Guid Id { get; set; }
        public int Qty { get; set; }
        public Guid? Order850_FK { get; set; }
        public virtual Order850DTO Order850 { get; set; }
        public virtual ICollection<ASNOrderDetail856DTO> ASNStoreDetail { get; set; }

       
        [ForeignKey("CartonInfoDTO")]
        public Guid? CartonInfo_FK { get; set; }
        public virtual CartonInfoDTO CartonInfoDTO { get; set; }


        [ForeignKey("StoreNotesDTO856")]
        public Guid? StoreNotesDTO856_FK { get; set; }
        public virtual StoreNotesDTO856 StoreNotesDTO856 { get; set; }

        

        public string  OrderStoreNumber  { get; set; }

        public string Barcode { get; set; }
        public decimal? Weight { get; set; }

        public string Label { get; set; }

     

    }
}
