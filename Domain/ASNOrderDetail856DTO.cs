﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("ASNOrderDetail856")]
    public  class ASNOrderDetail856DTO
    {
        public ASNOrderDetail856DTO()
        {
            SerialNumberInfoDTO = new HashSet<SerialNumberInfoDTO>();
        }
        public Guid Id { get; set; }
        public string DPCI { get; set; }
        public int QtyItem { get; set; }
        public int QtyPacked { get; set; }
        public int CustomerLineNumber { get; set; }
        public string ItemDescription { get; set; }
        public string UPC { get; set; }
        public string  VN { get; set; }
        [ForeignKey("Order850")]
        public Guid? Order850_FK { get; set; }
        public virtual Order850DTO Order850 { get; set; }
        public Guid? Carton856_FK { get; set; }
        public virtual Carton856DTO Carton856 { get; set; }
        public int FileStatus { get; set; }
        public virtual ICollection<SerialNumberInfoDTO > SerialNumberInfoDTO { get; set; }

    }
}
    