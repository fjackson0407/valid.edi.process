﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    [Table("StoreNotes856")]
    public  class StoreNotesDTO856
    {
        [Key]
        public Guid Id { get; set; }
        public string  Notes { get; set; }
    }
}
