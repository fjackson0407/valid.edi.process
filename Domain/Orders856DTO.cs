﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
 public  class Orders856DTO
    {
        public Orders856DTO()
        {
            StoreOrderDetail = new List<OrderDetail850DTO>();
        }

        public Guid ID { get; set; }
        public string StoreNumber { get; set; }
        public List<OrderDetail850DTO> StoreOrderDetail { get; set; }

    }
}
