﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
 public   class OrderInfo856DTO
    {
        public OrderInfo856DTO()
        {
            StoreDetail = new List<Domain.OrderDetail850DTO>();
            Cartons = new List<Domain.Carton856DTO>();
        }

        public Guid Id { get; set; }
        public string StoreNumber { get; set; }
        public string DCNumber { get; set; }
        public double PkgWeight { get; set; }
        public List<OrderDetail850DTO> StoreDetail { get; set; }

        public List<Carton856DTO> Cartons { get; set;  }
    }
}
