﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    [Table("PackWeight850")]
   public  class PackWeight850DTO
    {
        [Key]
        public Guid Id { get; set; }
        public string  CompanyCode  { get; set; }
        public float WeightofPack  { get; set; }


    }
}
