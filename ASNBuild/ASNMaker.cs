﻿using Common;
using Domain;
using OldRepository.DataSource;
using OldRepository.UOW;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using static Common.Mappers.Mapper;
using System.Diagnostics;


namespace VaildUSAASNBuild
{
    public class ASNMaker
    {

        private string m_sConnectionString { get; set; }
        private string m_sASNVisaPath { get; set; }
        private string m_sASNAmexPath { get; set; }
        public string m_sDate { get; set; }
        private IEnumerable<CartonInfoDTO> m_cBoxDetail;
        public Settings m_sSettings { get; private set; }

        public ASNMaker(Settings _Settings)
        {
            m_sSettings = _Settings;
            GetCartonDetail();
        }

        #region Write ASN to xml file 
        /// <summary>
        /// Write ASN file to output folder and save file database 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="Dc"></param>
        private void SaveDataToFile(XElement file, DC856 Dc)
        {

            var Setting = new XmlWriterSettings();
            Setting.Indent = true;
            Setting.NewLineOnAttributes = true;
            Setting.Encoding = Encoding.UTF8;
            Setting.WriteEndDocumentOnClose = true;
            StreamWriter cStreamWriter;
            string FileName;

            if (Dc.CompanyCode == "CER05")
            {
                FileName = string.Format("ASN Amex for DC {0} {1}", Dc.DcNumber, CommonFunctions.RandomString(ValidConstants.FileLength));
                cStreamWriter = File.CreateText(string.Format("{0}\\{1}{2}", m_sSettings.ASNFolderAmex, FileName, ValidConstants.XmlExtension));
            }
            else
            {
                FileName = string.Format("ASN McVisa for DC {0} {1}", Dc.DcNumber, CommonFunctions.RandomString(ValidConstants.FileLength));
                cStreamWriter = File.CreateText(string.Format("{0}\\{1}{2}", m_sSettings.ASNFolderVisaMaster, FileName, ValidConstants.XmlExtension));
            }
            using (XmlWriter cXmlWriter = XmlWriter.Create(cStreamWriter, Setting))
            {
                file.WriteTo(cXmlWriter);
            };
            using (var UoW = new UnitOfWork(new EDIContext(m_sSettings.ConnectionString)))
            {
                List<Order850DTO> Orders = UoW.Order.Find(t => t.DCNumber == Dc.DcNumber).ToList();
                Orders.ForEach(t => t.ASNStatus = (int)ASNStatus.HasASN);
                ASNFile856DTO ASNFile = new ASNFile856DTO();
                ASNFile.Id = Guid.NewGuid();
                ASNFile.DTS = DateTime.Now.Date;
                ASNFile.File = file.ToString();
                foreach (var _Order in Orders)
                {
                    ASNFile.Order.Add(_Order);

                }
                UoW.ASN.Add(ASNFile);
                int Result = UoW.Complete();
            }
        }


        #endregion

        #region Shipmednt level 
        private XElement BuildCarrier()
        {
            return new XElement(ValidConstants.Carrier,
                new XElement(ValidConstants.CarrierCode, new XCData(ValidConstants.NFIL)),
             new XElement(ValidConstants.CarrierType, ValidConstants.CarrierTypeVaule));
        }
        /// <summary>
        /// Return shipment element for ASN 
        /// </summary>
        /// <param name="Dc"></param>
        /// <param name="Address"></param>
        /// <param name="VendorCode"></param>
        /// <param name="DCNUmber"></param>
        /// <returns></returns>
        XElement MakeShipmentLevel(DC856 Dc)
        {
            using (var UoW = new UnitOfWork(new EDIContext(m_sSettings.ConnectionString)))
            {
                var Cartons = UoW.ASNOrderDetail.Find(t => t.Order850.DCNumber == Dc.DcNumber)
                                       .Select(t => t.Carton856);
                ShippingAddressInfoDTO Address = UoW.Order.Find(t => t.Id == Dc.ID).FirstOrDefault().AddressInfo;
                return new XElement(ValidConstants.ShipmentLevel,
                           new XElement(ValidConstants.TransactionSetPurposeCode, 00),
                           new XElement(ValidConstants.DATELOOP,
                           new XElement(ValidConstants.DateQualifier, ValidConstants.ShipDateDateQualifierNumber,
                           new XAttribute(ValidConstants.Desc, ValidConstants.ShipDateString)),
                           new XElement(ValidConstants.Date, m_sSettings.Shipdate)),
                           new XElement(ValidConstants.ManifestCreateTime, DateTime.Now.ToString("HH:mm")),
                           new XElement(ValidConstants.ShipmentTotals,
                           new XElement(ValidConstants.ShipmentTotalCube, ValidConstants.ShipmentTotalCubeValue),
                           new XElement(ValidConstants.ShipmentPackagingCode, m_sSettings.CartonTypes),
                           new XElement(ValidConstants.ShipmentTotalCases, Cartons.Count()),
                           new XElement(ValidConstants.ShipmentTotalWeight, Cartons.Count())),
                           BuildCarrier(), BuildBOL(Dc),
                           new XElement(ValidConstants.MethodOfPayment, ValidConstants.DF),
                           new XElement(ValidConstants.FOBLocationQualifier, "OR"),
                           new XElement(ValidConstants.FOBDescription, new XCData(ValidConstants.FOBDescription2)),
                           ValidAddress(), BuildshipTo(Address, Dc.DcNumber),
                           new XElement(ValidConstants.VendorCode, Dc.VendorNumber),
                           BuildContactElement());
            }

        }
        #endregion
        #region ASN functions 
        /// <summary>
        /// Get the sizes for each box 
        /// </summary>
        private void GetCartonDetail()
        {
            using (var UoW = new UnitOfWork(new EDIContext(m_sSettings.ConnectionString)))
            {
                m_cBoxDetail = UoW.CartonSize.GetAll();
            }
        }

        public void BuildASNForAllDC()
        {
            //IEnumerable<DC856> lisDC = new List<DC856>();
            List<DC856> lisDC = new List<DC856>();
            using (var UoW = new UnitOfWork(new EDIContext(m_sSettings.ConnectionString)))
            {

                var _lisdc = UoW.Order.Find(x => x.ASNStatus == (int)ASNStatus.ReadyForASN
                                   && x.PickStatus == (int)PickStatus.Open)
                                   .Select(x => new
                                   {
                                       x.DCNumber
                                   }).ToList().Distinct();

                foreach (var dc in _lisdc)
                {
                    lisDC.Add(UoW.Order.Find(x => x.ASNStatus == (int)ASNStatus.ReadyForASN
                                        && x.DCNumber == dc.DCNumber
                                        && x.PickStatus == (int)PickStatus.Open)
                                       .Select(x => new DC856()
                                       {
                                           ID = x.Id,
                                           PONumber = x.PONumber,
                                           PODate = x.PODate,
                                           PurchaseOrderSourceID = x.PurchaseOrderSourceID,
                                           CompanyCode = x.CompanyCode,
                                           DcNumber = x.DCNumber,
                                           VendorNumber = x.VendorNumber,
                                           CustomerNumber = x.CustomerNumber,

                                       }).FirstOrDefault());

                }
            }
            BuildASNForEachDC(lisDC);

        }

        private void GenerateCartonsForStores(IEnumerable<Order850DTO> OrderList)
        {
            var maxQtyPerBox = m_cBoxDetail.Max(t => t.CardCountMax);
            using (var UoW = new UnitOfWork(new EDIContext(m_sSettings.ConnectionString)))
            {
                foreach (var order in OrderList)
                {
                    var dbOrder = UoW.Order.Find(o => o.OrderStoreNumber == order.OrderStoreNumber && o.PONumber == order.PONumber).First();
                    dbOrder.Cartons = new List<Carton856DTO>();
                    //Starting a new carton for each order
                    var currentCarton = GetSSCC();
                    UoW.Carton.Add(currentCarton);
                    currentCarton.OrderStoreNumber = dbOrder.OrderStoreNumber;
                    // Estimate how many boxes we're expecting
                    var expectedBoxes = CalculateExpectedBoxes(dbOrder);

                    var asnDetailList = new List<ASNOrderDetail856DTO>();

                    foreach (var detail in dbOrder.OrderDetail)
                    {
                        var total = detail.QtyOrdered;
                        if (detail.QtyOrdered > maxQtyPerBox)
                        {
                            while (total > 0)
                            {
                                if (total >= maxQtyPerBox)
                                {
                                    asnDetailList.Add(new ASNOrderDetail856DTO()
                                    {
                                        Id = Guid.NewGuid(),
                                        DPCI = detail.DPCI,
                                        UPC = detail.UPC,
                                        Order850_FK = detail.Order850_FK,
                                        Order850 = detail.Order850,
                                        QtyItem = maxQtyPerBox,
                                        ItemDescription = detail.ItemDescription,
                                        FileStatus = (int)ASNStatus.ReadyForASN,
                                        VN = detail.VendorNumber
                                    });
                                    total -= maxQtyPerBox;
                                }
                                else if (total < maxQtyPerBox)
                                {
                                    asnDetailList.Add(new ASNOrderDetail856DTO()
                                    {
                                        Id = Guid.NewGuid(),
                                        DPCI = detail.DPCI,
                                        UPC = detail.UPC,
                                        Order850_FK = detail.Order850_FK,
                                        Order850 = detail.Order850,
                                        QtyItem = total,
                                        ItemDescription = detail.ItemDescription,
                                        FileStatus = (int)ASNStatus.ReadyForASN,
                                        VN = detail.VendorNumber
                                    });
                                    total -= detail.QtyOrdered;
                                }
                            }
                        }
                        else if (total <= maxQtyPerBox && total > 0)
                        {
                            asnDetailList.Add(new ASNOrderDetail856DTO()
                            {
                                Id = Guid.NewGuid(),
                                DPCI = detail.DPCI,
                                UPC = detail.UPC,
                                Order850_FK = detail.Order850_FK,
                                Order850 = detail.Order850,
                                QtyItem = total,
                                ItemDescription = detail.ItemDescription,
                                FileStatus = (int)ASNStatus.ReadyForASN,
                                VN = detail.VendorNumber
                            });
                        }
                    }

                    //Loop through the OrderDetails starting with the smallest qty lines
                    foreach (var detail in asnDetailList.OrderBy(x => x.QtyItem))
                    {

                        if (FitsInCarton(currentCarton, detail))
                        {
                            detail.Carton856_FK = currentCarton.Id;
                            currentCarton.ASNStoreDetail.Add(detail);
                            currentCarton.Qty += detail.QtyItem;
                        }
                        else
                        {
                            //Close box and start new one
                            dbOrder.Cartons.Add(currentCarton);
                            currentCarton = new Carton856DTO();
                            currentCarton = GetSSCC();
                            UoW.Carton.Add(currentCarton);
                            currentCarton.OrderStoreNumber = dbOrder.OrderStoreNumber;

                            detail.Carton856_FK = currentCarton.Id;
                            currentCarton.ASNStoreDetail.Add(detail);
                            currentCarton.Qty += detail.QtyItem;
                        }

                        UoW.ASNOrderDetail.Add(detail);
                    }
                    //done, add the carton to the order.
                    dbOrder.Cartons.Add(currentCarton);

                    try
                    {
                        UoW.Complete();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        throw;
                    }

                    //if (expectedBoxes != dbOrder.Cartons.Count)
                    //{
                    //    Debugger.Break();
                    //}

                    foreach (var carton in dbOrder.Cartons)
                    {
                        MinimizeBoxSize(carton);
                    }
                    try
                    {
                        UoW.Complete();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        throw;
                    }

                }
            }
        }

        private int CalculateExpectedBoxes(Order850DTO order)
        {
            var maxQtyPerBox = m_cBoxDetail.Max(t => t.CardCountMax);
            var sum = 0;
            foreach (var orderdetail in order.OrderDetail)
            {
                sum += orderdetail.QtyOrdered;
            }
            if (sum <= maxQtyPerBox)
                return 1;
            else
            {
                var numberofboxes = 0;
                while (sum > 0)
                {
                    sum -= maxQtyPerBox;
                    numberofboxes++;
                }
                return numberofboxes;
            }
        }

        private bool FitsInCarton(Carton856DTO carton, ASNOrderDetail856DTO orderDtl)
        {
            return carton.Qty + orderDtl.QtyItem <= m_cBoxDetail.Max(t => t.CardCountMax);
        }

        private void BuildASNForEachDC(IEnumerable<DC856> lisDC)
        {
            DC856 DC = new DC856();
            List<XElement> LisStoresForDC = new List<XElement>();
            if (!lisDC.Any())
            {
                //Throw error
            }
            int count = lisDC.Count();
            foreach (var _DC in lisDC)
            {

                var Header = HeaderSection(_DC);
                _DC.Stores850 = GetStoresForDC(_DC);

                GenerateCartonsForStores(_DC.Stores850);


                //This is the old way
                //foreach (var _stores in _DC.Stores850) 
                //{

                //    MakCartonsForEachOrder(_DC, _stores);
                //}
                var orders = BuildPickPackStructure(_DC);
                XElement ASNFile = new XElement(ValidConstants.FILE,
                new XElement(ValidConstants.DOCUMENT,
                Header,
                MakeShipmentLevel(_DC),
                orders));
                SaveDataToFile(ASNFile, _DC);
                count--;
            }
        }

        /// <summary>
        /// Build  a BOL number for the order and save to the database 
        /// </summary>
        /// <param name="DC"></param>
        /// <returns></returns>
        private XElement BuildBOL(DC856 DC)
        {
            int BOLNumber = GetNewBOLID();
            using (var UoW = new UnitOfWork(new EDIContext(m_sSettings.ConnectionString)))
            {
                var Order = UoW.Order.Find(t => t.Id == DC.ID).FirstOrDefault();
                BOL856DTO cBill = new Domain.BOL856DTO();
                cBill.ID = Guid.NewGuid();
                cBill.Order850_FK = Order.Id;
                cBill.Order850 = Order;
                cBill.BOLNumber = BOLNumber;
                UoW.BOL.Add(cBill);
                int Result = UoW.Complete();
            }
            return new XElement(ValidConstants.BillOfLadingNumber, new XCData(BOLNumber.ToString()));
        }
        private int GetNewBOLID()
        {
            Random cRandom = new Random();
            return cRandom.Next(100000, 100000000);
        }
        /// <summary>
        /// Contact info for shipment element 
        /// </summary>
        /// <returns></returns>
        private XElement BuildContactElement()
        {
            string[] ValidAddressParse = m_sSettings.ContactType.Split(',');
            return new XElement(ValidConstants.ContactType,
                   new XElement(ValidConstants.FunctionCode, new XCData(ValidConstants.CUST)),
                   new XElement(ValidConstants.ContactName, new XCData(ValidAddressParse[0])),
                   new XElement(ValidConstants.ContactQualifier, ValidConstants.Phone),
                   new XElement(ValidConstants.PhoneEmail, new XCData(ValidAddressParse[1])),
                   new XElement(ValidConstants.ContactQualifier, ValidConstants.Email),
                   new XElement(ValidConstants.PhoneEmail, new XCData(ValidAddressParse[2])));
        }

        private XElement BuildshipTo(ShippingAddressInfoDTO ShipTo, string DCNumber)
        {
            return new XElement(ValidConstants.NAME,
                   new XElement(ValidConstants.BillAndShipToCode, ValidConstants.ShipTo),
                   new XElement(ValidConstants.DUNSOrLocationNumberString, DCNumber),
                   new XElement(ValidConstants.NameComponentQualifier, ValidConstants.DescShipTo),
                   new XElement(ValidConstants.NameComponent, new XCData(string.Format("{0}    {1}", ValidConstants.SHIPPREDISTROTODC, DCNumber))),
                   new XElement(ValidConstants.CompanyName, new XCData(string.Format("{0}    {1}", ValidConstants.SHIPPREDISTROTODC, DCNumber))),
                   new XElement(ValidConstants.Address, new XCData(ShipTo.Address)),
                   new XElement(ValidConstants.City, new XCData(ShipTo.City)),
                   new XElement(ValidConstants.State, ShipTo.State),
                   new XElement(ValidConstants.Zip, ShipTo.PostalCode),
                   new XElement(ValidConstants.Country, ValidConstants.US));
        }

        /// <summary>
        /// Get the Address for Valid
        /// </summary>
        /// <returns></returns>
        private XElement ValidAddress()
        {
            string[] ValidAddressParse = m_sSettings.ValidAddress.Split(',');

            return new XElement(ValidConstants.NAME,
            new XElement(ValidConstants.BillAndShipToCode, ValidConstants.ShipFrom),
            new XElement(ValidConstants.DUNSOrLocationNumberString, ValidAddressParse[0]),
            new XElement(ValidConstants.NameComponentQualifier, ValidConstants.DescShipFrom),
            new XElement(ValidConstants.NameComponent, new XCData(ValidAddressParse[1])),
            new XElement(ValidConstants.CompanyName, new XCData(ValidAddressParse[1])),
            new XElement(ValidConstants.Address, new XCData(ValidAddressParse[2])),
            new XElement(ValidConstants.City, new XCData(ValidAddressParse[3])),
            new XElement(ValidConstants.State, ValidAddressParse[4]),
            new XElement(ValidConstants.Zip, ValidAddressParse[5].TrimEnd().TrimStart()));

        }
        /// <summary>
        ///  This function will make a cartons for the store order 
        /// </summary>
        /// <param name="Dc"></param>
        /// <param name="Store"></param>
        private void MakCartonsForEachOrder(DC856 Dc, Orders856DTO Store)
        {
            int MaxItemsForCarton = m_cBoxDetail.Max(t => t.CardCountMax);
            Carton856DTO Carton = GetSSCC();
            bool NewCarton = false;
            ASNOrderDetail856DTO cASNOrderDetail856 = new ASNOrderDetail856DTO();
            Order850DTO Order850 = null;
            Guid CartonType;
            int NumberOfMaxBoxes = 0;
            using (var UoW = new UnitOfWork(new EDIContext(m_sSettings.ConnectionString)))
            {
                int TotalQty = Store.StoreOrderDetail.Sum(t => t.QtyOrdered);

                foreach (OrderDetail850DTO _store in Store.StoreOrderDetail.OrderBy(x => x.QtyOrdered))
                {
                    if (_store != null)
                    {
                        var tempOrder850 = UoW.OrderDetail.Find(t => t.Order850.OrderStoreNumber == _store.Order850.OrderStoreNumber
                                            && t.ItemStatus == (int)ItemStatus.NotInCarton);
                        if (tempOrder850.Any())
                        {
                            Order850 = tempOrder850.FirstOrDefault().Order850;

                            #region Below box limit 
                            if (_store.QtyOrdered <= MaxItemsForCarton)
                            {
                                if (Carton.CartonInfo_FK == null || Carton.Qty + _store.QtyOrdered > MaxItemsForCarton)
                                {
                                    NewCarton = true;
                                    if (TotalQty > MaxItemsForCarton)
                                    {
                                        CartonType = GetBoxSize(MaxItemsForCarton);
                                    }
                                    else
                                    {
                                        CartonType = GetBoxSize(Store.StoreOrderDetail.Sum(T => T.QtyOrdered));
                                    }

                                    Carton = new Carton856DTO();
                                    Carton = GetSSCC();
                                    Carton.CartonInfo_FK = CartonType;
                                    Carton.Qty += _store.QtyOrdered;
                                    Carton.Weight += _store.QtyOrdered;
                                    Carton.OrderStoreNumber = _store.Order850.OrderStoreNumber;
                                }
                                cASNOrderDetail856.Id = Guid.NewGuid();
                                cASNOrderDetail856.UPC = _store.UPC;
                                cASNOrderDetail856.DPCI = _store.DPCI;
                                cASNOrderDetail856.VN = _store.VendorNumber;
                                cASNOrderDetail856.FileStatus = (int)ASNStatus.ReadyForASN;
                                cASNOrderDetail856.ItemDescription = _store.ItemDescription;
                                cASNOrderDetail856.QtyItem = _store.QtyOrdered;
                                cASNOrderDetail856.Carton856_FK = Carton.Id;
                                cASNOrderDetail856.Order850_FK = Order850.Id;
                                Carton.ASNStoreDetail.Add(cASNOrderDetail856);
                                // Set the item to In carton = yes 
                                var OrderItem = Order850.OrderDetail.First(t => t.Order850.OrderStoreNumber == _store.Order850.OrderStoreNumber);
                                var Order = UoW.OrderDetail.Find(t => t.Id == OrderItem.Id).FirstOrDefault();
                                Order.ItemStatus = (int)ItemStatus.InCarton;

                                if (NewCarton)
                                {
                                    UoW.Carton.Add(Carton);
                                    NewCarton = false;
                                }
                                else
                                {
                                    Carton.Qty += _store.QtyOrdered;
                                    Carton.Weight += _store.QtyOrdered;
                                    UoW.ASNOrderDetail.Add(cASNOrderDetail856);
                                }
                                int test = UoW.Complete();
                                cASNOrderDetail856 = new ASNOrderDetail856DTO();
                            }
                            #endregion
                            else
                            {
                                int Qty = _store.QtyOrdered;
                                #region case 1 
                                if (_store.QtyOrdered > MaxItemsForCarton)
                                {
                                    while (Qty > MaxItemsForCarton)
                                    {
                                        var BoxSize = GetBoxSize(MaxItemsForCarton);
                                        Carton = new Carton856DTO();
                                        Carton = GetSSCC();
                                        Carton.Qty = MaxItemsForCarton;
                                        Carton.Weight = MaxItemsForCarton;
                                        Carton.CartonInfo_FK = BoxSize;
                                        Carton.OrderStoreNumber = _store.Order850.OrderStoreNumber;

                                        cASNOrderDetail856 = new ASNOrderDetail856DTO
                                        {
                                            Id = Guid.NewGuid(),
                                            VN = _store.VendorNumber,
                                            UPC = _store.UPC,
                                            DPCI = _store.DPCI,
                                            FileStatus = (int)ASNStatus.ReadyForASN,
                                            ItemDescription = _store.ItemDescription,
                                            QtyItem = MaxItemsForCarton,
                                            Carton856_FK = Carton.Id,
                                            Carton856 = Carton,
                                            Order850_FK = Order850.Id
                                        };
                                        Carton.ASNStoreDetail.Add(cASNOrderDetail856);
                                        Order850.ASNStatus = (int)ASNStatus.InCarton;
                                        Qty -= MaxItemsForCarton;
                                        UoW.Carton.Add(Carton);
                                        int result = UoW.Complete();
                                        cASNOrderDetail856 = new ASNOrderDetail856DTO();
                                        ++NumberOfMaxBoxes;
                                    }
                                    if (Qty > 0)
                                    {
                                        cASNOrderDetail856 = new ASNOrderDetail856DTO();
                                        int BoxesWithMaxQty = NumberOfMaxBoxes * 2000;
                                        int NewQty = Store.StoreOrderDetail.Sum(T => T.QtyOrdered) - BoxesWithMaxQty;
                                        var BoxSize = GetBoxSize(NewQty);
                                        Carton = new Carton856DTO();
                                        Carton = GetSSCC();
                                        Carton.Id = Guid.NewGuid();
                                        Carton.Qty += NewQty;
                                        Carton.Weight += NewQty;
                                        Carton.CartonInfo_FK = BoxSize;
                                        Carton.OrderStoreNumber = _store.Order850.OrderStoreNumber;
                                        cASNOrderDetail856.FileStatus = (int)ASNStatus.ReadyForASN;
                                        cASNOrderDetail856.Id = Guid.NewGuid();
                                        cASNOrderDetail856.UPC = _store.UPC;
                                        cASNOrderDetail856.DPCI = _store.DPCI;
                                        cASNOrderDetail856.VN = _store.VendorNumber;
                                        cASNOrderDetail856.ItemDescription = _store.ItemDescription;
                                        cASNOrderDetail856.QtyItem = Qty;
                                        cASNOrderDetail856.Carton856_FK = Carton.Id;
                                        cASNOrderDetail856.Carton856 = Carton;
                                        cASNOrderDetail856.Order850_FK = Order850.Id;

                                        Order850.ASNStatus = (int)ASNStatus.InCarton;
                                        Carton.ASNStoreDetail.Add(cASNOrderDetail856);
                                        UoW.Carton.Add(Carton);
                                    }
                                    int Result = UoW.Complete();
                                    cASNOrderDetail856 = new ASNOrderDetail856DTO();
                                    #endregion
                                }
                            }
                        }

                    }
                }
            }

        }

        /// <summary>
        /// pick the size for the box based on the qty 
        /// </summary>
        /// <param name="qtyOrderedForStore"></param>
        /// <returns></returns>
        private Guid GetBoxSize(int qtyOrderedForStore)
        {
            return m_cBoxDetail.First(t => t.CardCountMin <= qtyOrderedForStore
                                && t.CardCountMax >= qtyOrderedForStore).ID;
        }

        private void MinimizeBoxSize(Carton856DTO carton)
        {
            var cartonInfo = m_cBoxDetail.First(t => t.CardCountMin <= carton.Qty && t.CardCountMax >= carton.Qty).ID;

            carton.CartonInfo_FK = cartonInfo;
        }

        /// <summary>
        /// Get a barcode for the next box 
        /// </summary>
        public Carton856DTO GetSSCC()
        {
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_sSettings.ConnectionStringForSSCC)))
            {
                
                Carton856DTO Carton = new Carton856DTO()
                {
                    Id = Guid.NewGuid(),
                    Barcode = UoW.SSCCBarcode.GetNextSequenceNumber(Helpers.EDIHelperFunctions.SSCCStatus.NotUsed),
                    CartonInfoDTO = null,
                    CartonInfo_FK = null,
                    Qty = 0,
                    Weight = 0


                };
                return Carton;
            }
        }
        /// <summary>
        /// Get all stores that belong to the DC 
        /// </summary>
        /// <param name="DC"></param>
        /// <returns></returns>
        private IEnumerable<Order850DTO> GetStoresForDC(DC856 DC)
        {

            using (var UoW = new UnitOfWork(new EDIContext(m_sSettings.ConnectionString)))
            {

                return UoW.Order.Find(t => t.DCNumber == DC.DcNumber &&
                                    t.ASNStatus == (int)ASNStatus.ReadyForASN &&
                                    t.PONumber == DC.PONumber && t.PODate == DC.PODate)
                                      .Select(t => new Order850DTO()
                                      {
                                          Id = t.Id,
                                          OrderStoreNumber = t.OrderStoreNumber,
                                          PONumber = t.PONumber,

                                          OrderDetail = UoW.OrderDetail.Find(g => g.Order850.OrderStoreNumber == t.OrderStoreNumber)
                                                                                 .Select(y => new OrderDetail850DTO()
                                                                                 {
                                                                                     Id = y.Id,
                                                                                     DPCI = y.DPCI,
                                                                                     UPC = y.UPC,
                                                                                     ItemStatus = (int)ItemStatus.NotInCarton,
                                                                                     ItemDescription = y.ItemDescription,
                                                                                     QtyOrdered = y.QtyOrdered,
                                                                                     VendorNumber = y.VendorNumber,
                                                                                     Order850 = y.Order850,
                                                                                     Order850_FK = y.Order850_FK
                                                                                 }).OrderBy(f => f.QtyOrdered).ToList()
                                      }).ToList().Distinct();
            }
        }

        #endregion
        #region Header element 
        /// <summary>
        /// This is the header section of the ASN
        /// </summary>
        /// <param name="DC"></param>
        /// <returns></returns>
        private XElement HeaderSection(DC856 DC)
        {
            string StoreNumberForHeaderInASN;

            using (var UoW = new UnitOfWork(new EDIContext(m_sSettings.ConnectionString)))
            {
                StoreNumberForHeaderInASN = UoW.Order.Find(t => t.Id == DC.ID).FirstOrDefault().OrderStoreNumber;
            }

            return new XElement("Header",
                         new XElement(ValidConstants.CompanyCodeName, DC.CompanyCode),
                         new XElement(ValidConstants.CUSTOMERNUMBER, DC.CustomerNumber),
                         new XElement(ValidConstants.DIRECTION, ValidConstants.OUTBOUND),
                         new XElement(ValidConstants.DocumentType, ValidConstants.DocumentTypeNumber),
                         new XElement(ValidConstants.Version, ValidConstants.VersionNumber),
                         new XElement(ValidConstants.FOOTPRINT, ValidConstants.ASN),
                         new XElement(ValidConstants.ShipmentID, string.Format("DC{0}{1}", DC.DcNumber, DC.ShipmentID)),
                         new XElement(ValidConstants.ShippingLocationNumber, new XCData(StoreNumberForHeaderInASN)));

        }
        #endregion
        #region Pick and Pack element 
        private List<XElement> BuildPickPackStructure(DC856 DC)
        {
            List<Guid?> cartons;
            List<Carton856DTO> lisCartons = new List<Carton856DTO>();
            int count = 0;
            List<XElement> LisOrders = new List<XElement>();

            using (var UoW = new UnitOfWork(new EDIContext(m_sSettings.ConnectionString)))
            {

                var Acartons = UoW.ASNOrderDetail.GetAll();
                cartons = UoW.ASNOrderDetail.Find(t => t.Order850.DCNumber == DC.DcNumber
                                             && t.FileStatus == (int)ASNStatus.ReadyForASN)

                                        .Select(t => t.Carton856_FK).Distinct().ToList();
                foreach (var item in cartons)
                {
                    lisCartons.Add(UoW.Carton.Find(t => t.Id == item).FirstOrDefault());
                }

                List<Carton856DTO> StoreCartons = new List<Carton856DTO>();
                foreach (var _carton in lisCartons)
                {

                    if (StoreCartons.Exists(t => t.Id == _carton.Id))
                    {
                        continue;
                    }
                    StoreCartons = UoW.Carton.Find(t => t.OrderStoreNumber == _carton.OrderStoreNumber).ToList();

                    if (StoreCartons.Count > 1)
                    {
                        LisOrders.Add(new XElement(ValidConstants.ORDERLEVEL,
                                  new XElement(ValidConstants.IDS,
                                   new XElement(ValidConstants.PurchaseOrderNumber, new XCData(_carton.ASNStoreDetail.FirstOrDefault().Order850.PONumber)),
                                    new XElement(ValidConstants.PURCHASEORDERSOURCEID, _carton.ASNStoreDetail.FirstOrDefault().Order850.DocumentId),
                                  new XElement(ValidConstants.StoreNumber, _carton.ASNStoreDetail.FirstOrDefault().Order850.OrderStoreNumber),
                                  new XElement(ValidConstants.DepartmentNumberString, "290"),
                                  new XElement(ValidConstants.DivisionNumberString, "290"),
                                  new XElement(ValidConstants.ReleaseNumberString, "D")),
                                  new XElement(ValidConstants.ORDERTOTALS,
                                  new XElement(ValidConstants.OrderTotalCases, StoreCartons.Count),
                                  new XElement(ValidConstants.OrderTotalWeight, lisCartons.Count)),
                                       from carton in StoreCartons
                                       select
                               new XElement(ValidConstants.PICKPACKSTRUTURE,
                               new XElement(ValidConstants.CARTON,
                               new XElement(ValidConstants.MARKS,
                               new XElement(ValidConstants.UCC128, new XCData(carton.Barcode))),
                               new XElement(ValidConstants.QUANTITIES,
                               new XElement(ValidConstants.QtyQualifier, "ZZ"),
                               new XElement(ValidConstants.QtyUOM, "ZZ"),
                               new XElement(ValidConstants.Qty, carton.Qty))),
                                  from cards in carton.ASNStoreDetail
                                  select
                                  new XElement(ValidConstants.ITEM,
                                  new XElement(ValidConstants.CustomerLineNumber, ++count),
                                  new XElement(ValidConstants.ITEMIDS,
                                  new XElement(ValidConstants.IdQualifier, "UP"),
                                  new XElement(ValidConstants.ID, new XCData(cards.UPC))),
                                  new XElement(ValidConstants.ITEMIDS,
                                  new XElement(ValidConstants.IDQUALIFIER, "VN"),
                                  new XElement(ValidConstants.ID, new XCData(cards.VN))),
                                  new XElement(ValidConstants.ITEMIDS,
                                  new XElement(ValidConstants.IDQUALIFIER, "CU"),
                                  new XElement(ValidConstants.ID, new XCData(cards.DPCI))),
                                  new XElement(ValidConstants.QUANTITIES,
                                  new XElement(ValidConstants.QtyQualifier, 39),
                                  new XElement(ValidConstants.QtyUOM, ValidConstants.Each),
                                  new XElement(ValidConstants.Qty, cards.QtyItem)),
                                  new XElement(ValidConstants.PackSize, 25),
                                  new XElement(ValidConstants.Inners, 1),
                                  new XElement(ValidConstants.EachesPerInner, 1),
                                  new XElement(ValidConstants.InnersPerPacks, 25),
                                  new XElement(ValidConstants.MEASUREMENT,
                                  new XElement(ValidConstants.MeasureQual, "WT"),
                                  new XElement(ValidConstants.MeasureValue, 0.038)),
                                  new XElement(ValidConstants.ItemDescription, cards.ItemDescription)))));

                    }
                    else
                    {
                        LisOrders.Add(new XElement(ValidConstants.ORDERLEVEL,
                                new XElement(ValidConstants.IDS,
                                 new XElement(ValidConstants.PurchaseOrderNumber, new XCData(_carton.ASNStoreDetail.FirstOrDefault().Order850.PONumber)),
                                  new XElement(ValidConstants.PURCHASEORDERSOURCEID, _carton.ASNStoreDetail.FirstOrDefault().Order850.DocumentId),
                                new XElement(ValidConstants.StoreNumber, _carton.ASNStoreDetail.FirstOrDefault().Order850.OrderStoreNumber),
                                new XElement(ValidConstants.DepartmentNumberString, "290"),
                                new XElement(ValidConstants.DivisionNumberString, "290"),
                                new XElement(ValidConstants.ReleaseNumberString, "D")),
                                new XElement(ValidConstants.ORDERTOTALS,
                                new XElement(ValidConstants.OrderTotalCases, StoreCartons.Count),
                                new XElement(ValidConstants.OrderTotalWeight, lisCartons.Count)),
                            new XElement(ValidConstants.PICKPACKSTRUTURE,
                             new XElement(ValidConstants.CARTON,
                             new XElement(ValidConstants.MARKS,
                             new XElement(ValidConstants.UCC128, new XCData(_carton.Barcode))),
                             new XElement(ValidConstants.QUANTITIES,
                             new XElement(ValidConstants.QtyQualifier, "ZZ"),
                             new XElement(ValidConstants.QtyUOM, "ZZ"),
                             new XElement(ValidConstants.Qty, _carton.Qty))),
                             from cards in _carton.ASNStoreDetail
                             select
                             new XElement(ValidConstants.ITEM,
                             new XElement(ValidConstants.CustomerLineNumber, ++count),
                             new XElement(ValidConstants.ITEMIDS,
                             new XElement(ValidConstants.IdQualifier, "UP"),
                             new XElement(ValidConstants.ID, new XCData(cards.UPC))),
                             new XElement(ValidConstants.ITEMIDS,
                             new XElement(ValidConstants.IDQUALIFIER, "VN"),
                             new XElement(ValidConstants.ID, new XCData(cards.VN))),
                             new XElement(ValidConstants.ITEMIDS,
                             new XElement(ValidConstants.IDQUALIFIER, "CU"),
                             new XElement(ValidConstants.ID, new XCData(cards.DPCI))),
                             new XElement(ValidConstants.QUANTITIES,
                             new XElement(ValidConstants.QtyQualifier, 39),
                             new XElement(ValidConstants.QtyUOM, ValidConstants.Each),
                             new XElement(ValidConstants.Qty, cards.QtyItem)),
                             new XElement(ValidConstants.PackSize, 25),
                             new XElement(ValidConstants.Inners, 1),
                             new XElement(ValidConstants.EachesPerInner, 1),
                             new XElement(ValidConstants.InnersPerPacks, 25),
                             new XElement(ValidConstants.MEASUREMENT,
                             new XElement(ValidConstants.MeasureQual, "WT"),
                             new XElement(ValidConstants.MeasureValue, 0.038)),
                             new XElement(ValidConstants.ItemDescription, cards.ItemDescription)))));
                    }
                    count = 0;

                }
                return LisOrders;
            }
        }
        
        #endregion
    }
}
