﻿using Common;
using Domain;
using LumenWorks.Framework.IO.Csv;
using OldRepository.DataSource;
using OldRepository.UOW;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Mappers.Mapper;

namespace EDIIntake
{
    public class EDI850ImportV2
    {

        public string m_sFile { get; private set; }
        private Settings _settings;


        public EDI850ImportV2()
        {

        }

        public EDI850ImportV2(Settings settings, string FileName )
        {
            m_sFile = FileName;
            _settings = settings;
        }

        /// <summary>
        /// See if the store is already in the database 
        /// </summary>
        /// <param name="PONumber"></param>
        /// <param name="Store"></param>
        /// <param name="DCNumber"></param>
        /// <returns></returns>
        public Order850DTO CheckForStore(string PONumber, string Store, string DCNumber)
        {
            using (var UoW = new UnitOfWork(new EDIContext(_settings.ConnectionString)))
            {
                return UoW.Order.Find(t => t.PONumber == PONumber
                                     && t.DCNumber == DCNumber
                                     && t.OrderStoreNumber == Store).FirstOrDefault();

            }
        }
        /// <summary>
        /// Parse the EDI file into the database. 
        /// this function will fill the store table and the store item table 
        /// </summary>
        public void ParseFile()
        {
            using (CsvReader csv = new CsvReader(new StreamReader(m_sFile),
                                    true, CsvReader.DefaultDelimiter,
                                    CsvReader.DefaultQuote,
                                    CsvReader.DefaultEscape,
                                    CsvReader.DefaultDelimiter, ValueTrimmingOptions.None))
            {
                List<Order850DTO> Stores = new List<Order850DTO>();
                csv.SupportsMultiline = true;
                IDataReader reader = csv;
                bool NewStore = false;
                Order850DTO Order = new Order850DTO();
                using (var UoW = new UnitOfWork(new EDIContext(_settings.ConnectionString)))
                {
                    while (reader.Read())
                    {
                        string PONumber = reader.GetValue((int)Inbound850Mapping.PONumber).ToString().Replace(@"'", "");
                        //TODO: Make sure the PO is not in the database 
                        string OrderStoreNumber = reader.GetValue((int)Inbound850Mapping.OrderStoreNumber).ToString();
                        string DCNumber = reader.GetValue((int)Inbound850Mapping.ShipToAddress).ToString().Replace(@"'", "");
                        Order = UoW.Order.Find(t => t.PONumber == PONumber
                                    && t.DCNumber == DCNumber
                                    && t.OrderStoreNumber == OrderStoreNumber).FirstOrDefault();
                        if (Order == null)
                        {
                            NewStore = true;
                            Order = new Order850DTO();
                            Order.Id = Guid.NewGuid();
                            Order.PONumber = PONumber;
                            Order.DCNumber = DCNumber;
                            Order.PODate = reader.GetDateTime((int)Inbound850Mapping.PODate);
                            Order.ASNStatus = (int)ASNStatus.ReadyForASN;
                            Order.DTS = DateTime.Now;
                            Order.DocumentId  = reader.GetValue((int)Inbound850Mapping.DocumentId).ToString();
                            Order.PickStatus = (int)PickStatus.Open;
                            Order.CustomerNumber = reader.GetValue((int)Inbound850Mapping.CustomerNumber).ToString();
                            Order.CompanyCode = reader.GetValue((int)Inbound850Mapping.CompanyCode).ToString();
                            Order.OrderStoreNumber = OrderStoreNumber;
                            Order.ShipDate = reader.GetDateTime((int)Inbound850Mapping.ShipDateOrder).ToString();
                            Order.VendorNumber = reader.GetValue((int)Inbound850Mapping.VendorNumber).ToString();
                            Order.PurchaseOrderSourceID = reader.GetValue((int)Inbound850Mapping.DocumentId).ToString();
                            var Dc = UoW.ShippingAddress.Find(t => t.StoreID == DCNumber).FirstOrDefault();
                            Order.AddressInfo = Dc;
                            Order.AddressInfo_FK = Dc.Id;
                            Order.AddressInfo = Dc;

                        }
                        OrderDetail850DTO OrderDetail = new OrderDetail850DTO();
                        OrderDetail.Id = Guid.NewGuid();
                        OrderDetail.Order850_FK = Order.Id;
                        OrderDetail.Order850 = Order;
                        var PackWeight = UoW.PackWeight.Find(t => t.CompanyCode == Order.CompanyCode).FirstOrDefault();
                         OrderDetail.PackInfo = PackWeight;
                        OrderDetail.PackInfo_FK = PackWeight.Id;
                        OrderDetail.ItemStatus = (int)ItemStatus.NotInCarton;
                        OrderDetail.ItemDescription = reader.GetValue((int)Inbound850Mapping.ItemDescription).ToString();
                        OrderDetail.UPC = reader.GetValue((int)Inbound850Mapping.UPCCode).ToString().Replace(@"'", "");
                        OrderDetail.DPCI = reader.GetValue((int)Inbound850Mapping.DPCI).ToString();
                        OrderDetail.QtyOrdered = reader.GetInt32((int)Inbound850Mapping.QtyOrdered);
                        OrderDetail.VendorNumber = reader.GetValue((int)Inbound850Mapping.VendorItemNo).ToString();
                        if (NewStore)
                        {
                            UoW.Order.Add(Order);
                            NewStore = false;
                        }
                        Order.OrderDetail.Add(OrderDetail);
                        UoW.Complete();
                        Order = new Order850DTO();
                    }
                }
            }

        }
    }

}