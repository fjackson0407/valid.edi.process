﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using OldRepository.UOW;
using Domain;
using OldRepository.DataSource;
using Common;
using System.IO;
using OfficeOpenXml.Style;
namespace OrderReports
{
    public class WeeklyReports
    {
        #region Class Members
        private Settings _settings;
        #endregion
        #region Constructor

        public WeeklyReports(Settings settings)
        {
            _settings = settings;
        }

        #endregion
        public void GetPOForTheWeek(DateTime PODate)
        {
            Order850DTO Order = new Order850DTO();
            List<Carton> lisCarton = new List<Carton>();
            List<CartonInfoDTO> CartonSizes = new List<CartonInfoDTO>();
            List<string> PONumbers = new List<string>();
            List<BOLSummary> BOLSummary = new List<BOLSummary>();
            
            using (var UoW = new UnitOfWork(new EDIContext(_settings.ConnectionString)))
            {
                var Orders = UoW.Order.Find(t => t.PODate == PODate).ToList();
                //var test2 = Orders.Find(t => t.PONumber == "0290-3160600-3804").ASNFile856DTO;
                //var test = Orders.Find(t => t.PONumber == "0290-3160600-0559").ASNFile856DTO;
                //var test3 = Orders.Find(t => t.PONumber == "0290-3160600-0553").ASNFile856DTO;
                CartonSizes = UoW.CartonSize.GetAll().ToList();
                
                foreach (var item in Orders)
                {

                    lisCarton.AddRange(UoW.ASNOrderDetail.Find(t => t.Order850_FK == item.Id)
                                                  .Select(t => new Carton()
                                                  {
                                                       Id = t.Carton856.CartonInfo_FK,
                                                      Size = t.Carton856.CartonInfoDTO.Size,
                                                      Author = t.Carton856.CartonInfoDTO.Author,
                                                      SKU = t.Carton856.CartonInfoDTO.SKU,
                                                      Min = t.Carton856.CartonInfoDTO.CardCountMin,
                                                      Max = t.Carton856.CartonInfoDTO.CardCountMax,
                                                     PONumber = t.Order850.PONumber,
                                                      CardType = t.Order850.CompanyCode

                                                  }
                                                  ));
                    
                }                     
            }
            //Testing this should be 25 but I am getting 23 
           var  _PONumbers = lisCarton.Select(t => t.PONumber).ToList();
            var BoxTypes = lisCarton.Select(t => t.Id).ToList();
            PONumbers = lisCarton.Select(t => t.PONumber).Distinct().ToList();
            foreach (var item in PONumbers )
            {

                
                BOLSummary.Add(lisCarton.Where(t => t.PONumber == item).Distinct()
                                             .Select(t => new BOLSummary()
                                             {
                                                 PONumber = t.PONumber,
                                                 BoxCount = lisCarton.Count(y => y.PONumber == item),
                                                 Weight = lisCarton.Count(y => y.PONumber == item),
                                                  CardType = t.CardType 
                                             }).FirstOrDefault());
            }

            MakeReport(BOLSummary ,  CartonSizes, BoxTypes  );
        }

        public void MakeReport(List<BOLSummary> Bols, List<CartonInfoDTO> CartonSizes , List<Guid?> BoxTypes )
        {
            int BOLSummaryAmexCol = 1;
            int BOLSummaryVisaMcCol = 9;
            int BOLRow = 8;
            int BOLVisaRow = 8; 
            
            using (ExcelPackage cExcelPackage = new ExcelPackage(new FileInfo(_settings.ReportFolder)))
            {
                ExcelWorksheet xWSAmexBOLSummary = cExcelPackage.Workbook.Worksheets.Add("Visa Mastercard BOL");
                #region Amex
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryAmexCol].Value = ValidConstants.BOLSummary;
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryAmexCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryAmexCol].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryAmexCol].Style.Font.Size = 14;
                xWSAmexBOLSummary.Cells[BOLRow, ++BOLSummaryAmexCol].Value = "Boxes";
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryAmexCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryAmexCol].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryAmexCol].Style.Font.Size = 14;
                xWSAmexBOLSummary.Cells[BOLRow, ++BOLSummaryAmexCol].Value = "LBS";
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryAmexCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryAmexCol].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryAmexCol].Style.Font.Size = 14;
                #endregion
                #region Visa Master Card
                BOLSummaryAmexCol = 8;
                BOLRow = 8;
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryVisaMcCol].Value = ValidConstants.BOLSummary;
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryVisaMcCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryVisaMcCol].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryVisaMcCol].Style.Font.Size = 14;
                xWSAmexBOLSummary.Cells[BOLRow, ++BOLSummaryVisaMcCol].Value = "Boxes";
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryVisaMcCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryVisaMcCol].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryVisaMcCol].Style.Font.Size = 14;
                xWSAmexBOLSummary.Cells[BOLRow, ++BOLSummaryVisaMcCol].Value = "LBS";
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryVisaMcCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryVisaMcCol].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);
                xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryVisaMcCol].Style.Font.Size = 14;
                #endregion

                ++BOLRow;
                BOLSummaryAmexCol = 1;
                xWSAmexBOLSummary.Cells[1, 1].Value = ValidConstants.Page;
                xWSAmexBOLSummary.Cells[3, 3].Style.Font.Bold = true;
                xWSAmexBOLSummary.Cells[3, 1].Value = ValidConstants.BOLDdate;
                xWSAmexBOLSummary.Cells[3, 3].Value = DateTime.Now.ToString();
                xWSAmexBOLSummary.Cells[4, 1].Value = ValidConstants.BOLNumber;
                xWSAmexBOLSummary.Cells[4, 3].Value = string.Format("{0} -{1}", DateTime.Now.ToString(),
                                                     ValidConstants.combined);
                int Width = 1;
                xWSAmexBOLSummary.Cells[4, 3].Style.Font.Bold = true;
                xWSAmexBOLSummary.Cells[5, 3].Style.Font.Bold = true;
                xWSAmexBOLSummary.Cells[5, 3].Value = "(Amex)";
                xWSAmexBOLSummary.Cells.Style.Font.Name = "Arial";
                xWSAmexBOLSummary.Column(Width).Width = 30;
                xWSAmexBOLSummary.Column(++Width).Width = 30;
                xWSAmexBOLSummary.Column(++Width).Width = 30;

                BOLSummaryVisaMcCol = 9;
                xWSAmexBOLSummary.Cells[4, BOLSummaryVisaMcCol ].Style.Font.Bold = true;
                xWSAmexBOLSummary.Cells[5, BOLSummaryVisaMcCol].Style.Font.Bold = true;
                xWSAmexBOLSummary.Cells[5, BOLSummaryVisaMcCol].Value = "(Visa/MC)";
                Width = 9;
                xWSAmexBOLSummary.Column(Width).Width = 30;
                xWSAmexBOLSummary.Column(++Width).Width = 30;
                xWSAmexBOLSummary.Column(++Width).Width = 30;
                BOLSummaryAmexCol = 1;
                BOLVisaRow = 9;
                BOLSummaryVisaMcCol = 9;
                foreach (var _PoSummary in Bols)
                {
                    if (_PoSummary.CardType == "CER05") //Amex
                    {
                        xWSAmexBOLSummary.Cells[BOLRow, BOLSummaryAmexCol].Value = _PoSummary.PONumber;
                        xWSAmexBOLSummary.Cells[BOLRow, ++BOLSummaryAmexCol].Value = _PoSummary.BoxCount;
                        xWSAmexBOLSummary.Cells[BOLRow, ++BOLSummaryAmexCol].Value = _PoSummary.BoxCount;
                        ++BOLRow;
                    }
                    else //VisaMc
                    {

                        xWSAmexBOLSummary.Cells[BOLVisaRow , BOLSummaryVisaMcCol ].Value = _PoSummary.PONumber;
                        xWSAmexBOLSummary.Cells[BOLVisaRow , ++BOLSummaryVisaMcCol ].Value = _PoSummary.BoxCount;
                        xWSAmexBOLSummary.Cells[BOLVisaRow , ++BOLSummaryVisaMcCol].Value = _PoSummary.BoxCount;
                        ++BOLVisaRow;
                    }
                    BOLSummaryAmexCol = 1;
                    BOLSummaryVisaMcCol = 9;

                }
                //Break down of box sizes 
                BOLVisaRow = 9;
                BOLSummaryVisaMcCol = 13;
                xWSAmexBOLSummary.Cells[BOLVisaRow, BOLSummaryVisaMcCol ].Style.Font.Bold = true;
                xWSAmexBOLSummary.Column(BOLSummaryVisaMcCol ).Width = 30;
                var CartonInOrder =   CartonSizes.OrderBy(t => t.CardCountMin);
                foreach (var item in CartonInOrder )
                {
                    xWSAmexBOLSummary.Cells[BOLVisaRow, BOLSummaryVisaMcCol].Value = $"{item.BundleCountMin } - {item.BundleCountMax}";
                    xWSAmexBOLSummary.Cells[BOLVisaRow, BOLSummaryVisaMcCol].Style.Font.Bold = true;
                    xWSAmexBOLSummary.Column(BOLSummaryVisaMcCol).Width = 30;

                    var BoxCountPerBreakDown = BoxTypes.Where(t => t.Value == item.ID);
                    xWSAmexBOLSummary.Cells[BOLVisaRow, ++BOLSummaryVisaMcCol].Value = $"{BoxCountPerBreakDown.Count()}";
                    xWSAmexBOLSummary.Cells[BOLVisaRow, BOLSummaryVisaMcCol].Style.Font.Bold = true;
                    xWSAmexBOLSummary.Column(BOLSummaryVisaMcCol).Width = 30;

                    ++BOLVisaRow;
                    BOLSummaryVisaMcCol = 13; 
                }
                cExcelPackage.Save();
            }
        }
    }
}
