﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using OldRepository.Interfaces;

namespace OldRepository.Implementation
{
 public    class ASNOrderDetail : RepositoryBase<ASNOrderDetail856DTO>, IASNOrderDetail
    {

        public ASNOrderDetail(EDIContext context)
            :base(context ) { }
    }
}
