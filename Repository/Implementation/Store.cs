﻿using Domain;
using Repository.BaseClass;
using Repository.DataSource;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implementation
{
  public   class Store : RepositoryBase<Order850DTO>, IStore
    {

        public Store(EDIContext context)
            : base(context) 
        { }

        public int SaveChange()
        {
            return this.Context.SaveChanges();


        }
    }
}
