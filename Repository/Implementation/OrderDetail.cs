﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using OldRepository.Interfaces;

namespace OldRepository.Implementation
{
  public   class OrderDetail : RepositoryBase<OrderDetail850DTO> , IOrderDetail
    {
        public OrderDetail(EDIContext context)
            : base(context) { }
    }
}
