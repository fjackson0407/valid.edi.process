﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using OldRepository.Interfaces;

namespace OldRepository.Implementation
{
   public  class CartonInfo : RepositoryBase<CartonInfoDTO>, ICartonInfo
    {
            public CartonInfo(EDIContext context)
            : base(context) { }
    }
}
