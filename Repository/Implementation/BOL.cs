﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using OldRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.Implementation
{
 public   class BOL : RepositoryBase<BOL856DTO> , IBOL
    {
        public BOL(EDIContext context)
            : base(context) 
        { }

    }
}
