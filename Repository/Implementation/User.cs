﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using OldRepository.Interfaces;

namespace OldRepository.Implementation
{
    public class User : RepositoryBase<UserInfoDTO>, IUser
    {
        public User(EDIContext Context)
            : base(Context) { }
    }
}
