﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using OldRepository.Interfaces;

namespace OldRepository.Implementation
{
    public class ShippingAddress : RepositoryBase<ShippingAddressInfoDTO>, IShippingAddress
    {
        public ShippingAddress(EDIContext context)
            :base(context ) { }
    }
}
