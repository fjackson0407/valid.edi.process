﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using OldRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Mappers.Mapper;
using static Common.ValidConstants;

namespace OldRepository.Implementation
{
 public   class SSCC  : RepositoryBase<SSCC856DTO>, ISSCC
    {

        public SSCC(EDIContext context)
            : base(context) 
        { }

        public SSCC856DTO GetNextSequenceNumber(SSCCStatus bUsed)
        {
            List<SSCC856DTO> First = this.GetAll().ToList();
            SSCC856DTO  cSSCC = null;
            if (First.Count == 0)
            {
                cSSCC = new SSCC856DTO();
                {
                    cSSCC.Id = Guid.NewGuid();
                    cSSCC.DTS = DateTime.Now;
                    cSSCC.SequenceNumber = 1;
                    cSSCC.Used = (int)SSCCStatus.NotUsed;
                }

            }
            else
            {
                cSSCC = this.Find(T => T.Used == (int)bUsed).FirstOrDefault();

            }

            SSCC856DTO  cSSCCNew = new SSCC856DTO();
            string sBarcode;
            sBarcode = string.Empty;
            sBarcode = SetBarcode(EXTENSIONDIGIT, COMPANYCODE, cSSCC.SequenceNumber.ToString());
            int iEven = 0;
            int iOdd = 0;
            int iPosition = 1;
            int iTotal = 0;
            int iDelta = 0;

            if (sBarcode.Length == SSCCLENGTH)
            {

                foreach (char item in sBarcode)
                {
                    if ((iPosition % 2) == 0)
                    {
                        iEven += int.Parse(item.ToString());
                    }
                    else
                    {
                        iOdd += int.Parse(item.ToString());
                    }
                    iPosition++;
                }
                iOdd *= 3;
                iTotal = iEven + iOdd;

                while ((iTotal % 10) != 0)
                {
                    iTotal++;
                    iDelta++;

                }

            }
            cSSCC.Used = (int)SSCCStatus.Used;
            if (First.Count == 0)
            {

                this.Add(cSSCC);
            }
            else
            {
                this.SaveChange();
            }

            cSSCCNew = NewSequenceNumber(cSSCC.SequenceNumber);
            this.Add(cSSCCNew);
            this.Context.SaveChanges();

            sBarcode += iDelta;
            int LengthBefore = sBarcode.Length;
            sBarcode = APPLICATINIDENTIFER + sBarcode;
            int lengthAfter = sBarcode.Length;

            cSSCC.BarCode = sBarcode;
            return cSSCC;
        }

        public string GetNextSequenceNumber(string ucc128 )
        {
            string sBarcode = SetBarcode(Common.ValidConstants.EXTENSIONDIGIT,
                                          COMPANYCODE,
                                             ucc128);
            int iEven = 0;
            int iOdd = 0;
            int iPosition = 1;
            int iTotal = 0;
            int iDelta = 0;

            if (sBarcode.Length == SSCCLENGTH)
            {

                foreach (char item in sBarcode)
                {
                    if ((iPosition % 2) == 0)
                    {
                        iEven += int.Parse(item.ToString());
                    }
                    else
                    {
                        iOdd += int.Parse(item.ToString());
                    }
                    iPosition++;
                }
                iOdd *= 3;
                iTotal = iEven + iOdd;

                while ((iTotal % 10) != 0)
                {
                    iTotal++;
                    iDelta++;

                }

            }

            sBarcode += iDelta;
            int LengthBefore = sBarcode.Length;
            sBarcode = APPLICATINIDENTIFER + sBarcode;
            int lengthAfter = sBarcode.Length;
            return sBarcode;

        }
        
        public SSCC856DTO NewSequenceNumber(int sequenceNumber)
        {
            SSCC856DTO cSSCC = new SSCC856DTO();
            cSSCC.Id = Guid.NewGuid();
            cSSCC.Used = SCCNUMBERNOTUSED;
            cSSCC.DTS = DateTime.Now;
            cSSCC.SequenceNumber = sequenceNumber + 1;
            return cSSCC;
        }

        public int SaveChange()
        {
            return this.Context.SaveChanges();
        }

        private string SetBarcode(string ExtenstionDegit, string CompanyCode, string SequenceNumber)
        {
            string sResult = string.Empty;
            const string ZERO = "0";

            if (string.IsNullOrEmpty(ExtenstionDegit) || string.IsNullOrEmpty(CompanyCode) || string.IsNullOrEmpty(SequenceNumber))
            {
                sResult = string.Empty; //Throw error 
            }
            else
            {
                while (ExtenstionDegit.Length + CompanyCode.Length + SequenceNumber.Length != SSCCLENGTH)
                {
                    SequenceNumber = ZERO + SequenceNumber;
                }
                sResult = ExtenstionDegit + CompanyCode + SequenceNumber;
            }
            return sResult;
        }


    }
}
