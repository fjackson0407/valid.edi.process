﻿using Domain;
using Repository.BaseClass;
using Repository.DataSource;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implementation
{
   public  class ShipTo : RepositoryBase<DCInformationDTO> , IShipTo
    {
        public ShipTo(EDIContext context)
            :base(context ) { }
    }
}
