﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using OldRepository.Interfaces;

namespace OldRepository.Implementation
{
public   class ASN : RepositoryBase<ASNFile856DTO>, IASN
    {

        public ASN(EDIContext context)
            : base(context) 
        { }

    }
}
