﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using OldRepository.Interfaces;

namespace OldRepository.Implementation
{
public   class PackWeight : RepositoryBase<PackInfoDTO>, IPackWeight
    {
        public PackWeight(EDIContext context)
            : base(context) { }

    }
}
