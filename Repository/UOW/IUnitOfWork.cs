﻿using OldRepository.Interfaces;
using System;

namespace OldRepository.UOW
{
   public  interface IUnitOfWork : IDisposable
    {
        IASN ASN { get;  }
        ICarton Carton { get; }
        IOrderDetail OrderDetail { get;  }
        IOrders Order { get; }
        ICartonInfo CartonSize { get; }
        IASNOrderDetail ASNOrderDetail { get;  }
        IPackWeight PackWeight { get;  }
        IShippingAddress ShippingAddress { get;  }
        IUser User { get;  }
        ISSCC SSCC { get;  }
        IBOL BOL { get;  }
        int Complete();
    }
}
