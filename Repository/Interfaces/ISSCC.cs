﻿using Domain;
using OldRepository.BaseClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Mappers.Mapper;

namespace OldRepository.Interfaces
{
    public   interface ISSCC : IRepositoryBase<SSCC856DTO>
    {
        SSCC856DTO GetNextSequenceNumber(SSCCStatus used);
        string GetNextSequenceNumber(string ucc128);
        SSCC856DTO NewSequenceNumber(int sequenceNumber);
        int SaveChange();
    }
}
