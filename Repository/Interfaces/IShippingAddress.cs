﻿using Domain;
using OldRepository.BaseClass;

namespace OldRepository.Interfaces
{
    public interface IShippingAddress : IRepositoryBase<ShippingAddressInfoDTO>
    {
    }
}
