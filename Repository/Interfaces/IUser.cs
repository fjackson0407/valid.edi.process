﻿using Domain;
using OldRepository.BaseClass;

namespace OldRepository.Interfaces
{
    public interface IUser : IRepositoryBase<UserInfoDTO>
    {
    }
}
