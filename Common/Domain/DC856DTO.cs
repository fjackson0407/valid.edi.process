﻿using System;
using System.Collections.Generic;

namespace Domain
{
   public  class DC856DTO
    {
        public Guid ID { get; set; }
        public string DcNumber { get; set; }
        public string StoreNumber { get; set; }
        public string CompanyCode { get; set; }
        public string CustomerNumber { get; set; }
        public string DocumentId { get; set; }
        public DateTime PODate { get; set; }
        public string PONumber  { get; set; }
        public string PurchaseOrderSourceID { get; set; }
        public string VendorNumber { get; set; }
        public IEnumerable<ASNOrderInfo856DTO> AsnOrdersForDC { get; set; }
        public virtual ShippingAddressInfoDTO AddressInfo { get; set; }
    }
}
