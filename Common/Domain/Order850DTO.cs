﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("Order850")]
    public class Order850DTO
    {
        public Order850DTO()
        {
            ASN = new HashSet<ASNFile856DTO>();
            BOL = new HashSet<BOL856DTO>();
            OrderDetail = new HashSet<OrderDetail850DTO>();
            ASNOrderDetail = new HashSet<ASNOrderDetail856DTO>();
        }
        public virtual ICollection<ASNFile856DTO> ASN { get; set; }
        public virtual ICollection<BOL856DTO> BOL { get; set; }
        public virtual ICollection<OrderDetail850DTO> OrderDetail { get; set; }
        public virtual ICollection<ASNOrderDetail856DTO> ASNOrderDetail { get; set; }

        [Key]
        public Guid Id { get; set; }
        public DateTime DTS { get; set; }
        public string CompanyCode { get; set; }
        public string VendorNumber { get; set; }
        public string CustomerNumber { get; set; }
        public string PONumber { get; set; }
        public string OrderStoreNumber { get; set; }
        public DateTime PODate { get; set; }
        public string ShipDate { get; set; }
        public string CancelDate { get; set; }
        public string DCNumber { get; set; }
        public string BillToAddress { get; set; }
        public string DocumentId { get; set; }
        public int ASNStatus { get; set; }
    }
}
