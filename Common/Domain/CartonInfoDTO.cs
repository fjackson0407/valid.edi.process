﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("CartonInfo")]
    public class CartonInfoDTO
    {
        public CartonInfoDTO()
        {
            
        }

        [Key]
        public Guid ID { get; set; }
        
        [NotMapped]
        public int BundleCountMin
        {
            get
            {
                return CardCountMin / 25;
            }
        }

        [NotMapped]
        public int BundleCountMax
        {
            get
            {
                return CardCountMax / 25;
            }
        }
        public int CardCountMin { get; set; }
        public int CardCountMax { get; set; }
        public string Size { get; set; }
        public string SKU { get; set; }
        public string Author { get; set; }
        public DateTime DTS { get; set; }
 
    }
}
