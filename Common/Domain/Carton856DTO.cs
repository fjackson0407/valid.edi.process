﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("Carton856")]
    
    public class Carton856DTO
    {
        public Carton856DTO()
        {
            SerialNumbers = new HashSet<SerialNumberInfoDTO>();
        }
        public virtual ICollection<SerialNumberInfoDTO> SerialNumbers { get; set; }

        [Key]
        public Guid Id { get; set; }
        public int Qty { get; set; }

        [ForeignKey("ASNOrderDetail")]
        public Guid? ASNOrderDetail856_FK { get; set; }
        public virtual ASNOrderDetail856DTO ASNOrderDetail { get; set; }

        public Guid? CartonInfo_FK { get; set; }
        public virtual CartonInfoDTO CartonInfo { get; set; }

        public string Barcode { get; set; }
        public int BarcodeSeqNumber { get; set; }
        public decimal? Weight { get; set; }
        


    }
}
