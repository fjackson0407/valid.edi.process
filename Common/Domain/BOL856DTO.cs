﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("BOL856")]
    public  class BOL856DTO
    {
        public Guid ID { get; set; }
        public int BOLNumber { get; set; }

        [ForeignKey("Order850")]
        public Guid? Order850_FK { get; set; }
        public virtual Order850DTO Order850 { get; set; }

    }
}
