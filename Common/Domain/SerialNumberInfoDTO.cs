﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("SerialNumberInfo")]
    public   class SerialNumberInfoDTO
    {
        public Guid ID { get; set; }
        public Int64 SerialNumber { get; set; }
        public string Serialbundle { get; set; }
        
        [ForeignKey("Carton856")]
        public Guid? Carton856_FK { get; set;  }
        public virtual Carton856DTO Carton856 { get; set; }
    }
}
