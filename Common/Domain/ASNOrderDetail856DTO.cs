﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("ASNOrderDetail856")]
    public  class ASNOrderDetail856DTO
    {
        public ASNOrderDetail856DTO()
        {
            Cartons = new HashSet<Carton856DTO>();
        }

        public virtual ICollection<Carton856DTO> Cartons { get; set;  }

        public string OrderNotes { get; set; }
        public Guid Id { get; set; }
        public string DPCI { get; set; }
        public string OrderStoreNumber { get; set; }
        public int Qty { get; set; }
        public int CustomerLineNumber { get; set; }
        public double PkgWeight { get; set; }
        public string ItemDescription { get; set; }
        public string UPC { get; set; }
        public string  VN { get; set; }
        public string PONumber { get; set; }
        [NotMapped]
        public string POSubstring
        {
            get
            {
                string[] parts = PONumber.Split('-');
                return string.Format("{0}-{1}", parts[0], parts[1]);
            }
        }
        public string Label { get; set; }
        public int PickStatus { get; set; }
        public string UserName { get; set; }
        public string InUseBy { get; set; }
        public DateTime? OrderCompleteTime { get; set; }

        [ForeignKey("PackInfo")]
        public Guid? PackInfo_FK { get; set; }
        public virtual PackInfoDTO PackInfo { get; set; }

        [ForeignKey("Order850")]
        public Guid? Order850_FK { get; set; }
        public virtual Order850DTO Order850 { get; set; }

    }
}
    