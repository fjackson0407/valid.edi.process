﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("ASNFile856")]
  public  class ASNFile856DTO
    {
        public Guid Id { get; set; }
        public string File { get; set; }
        public DateTime DTS { get; set; }

        [ForeignKey("Order850")]
        public Guid? Order850_FK { get; set; }
        public virtual Order850DTO Order850 { get; set; }
    }
}
