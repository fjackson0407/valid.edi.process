﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("OrderDetail850")]
    public  class OrderDetail850DTO
    {
        //public OrderDetail850DTO()
        //{
        //    Cartons = new HashSet<Carton856DTO>();
        //}
        //public virtual ICollection<Carton856DTO> Cartons { get; set; }

        public Guid Id { get; set; }
        public string DPCI { get; set; }
        public int QtyOrdered { get; set; }
        public int CustomerLineNumber { get; set; }
        public string ItemDescription { get; set; }
        public string UPC { get; set; }
        public string VN { get; set; }
        public string OrderStoreNumber { get; set; }

        [ForeignKey("Order850")]
        public Guid? Order850_FK { get; set; }
        public virtual Order850DTO Order850 { get; set; }

        //[ForeignKey("PackInfo")]
        //public Guid? PackInfo_FK { get; set; }
        //public virtual PackInfoDTO PackInfo { get; set; }

        [ForeignKey("AddressInfo")]
        public Guid? AddressInfo_FK { get; set; }
        public virtual ShippingAddressInfoDTO AddressInfo { get; set; }
        
        public string VendorNumber { get; set; }
        public bool ItemInCarton { get; set; }


    }
}
