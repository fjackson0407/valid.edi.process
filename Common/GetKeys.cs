﻿using Domain;
using Helpers;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValidUSAEDI;
using Microsoft.Exchange.WebServices.Data;
using System.Net;
using System.Net.Mail;
using Microsoft.Exchange.WebServices;
using EDIException;
using static Helpers.EDIHelperFunctions;

namespace RegistryFunctions
{
    public class GetKeys
    {
        public bool HSA
        {
            get
            {
                int iHSA = 0;
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);

                if (reg64 != null)
                {
                    object oHSA = reg64.GetValue(EDIHelperFunctions.HSA, EDIHelperFunctions.HSA_NOT_FOUND, RegistryValueOptions.None);
                    RegistryValueKind cRegistryValueKind = reg64.GetValueKind(EDIHelperFunctions.HSA);
                    iHSA = (int)oHSA;
                    if (iHSA == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    throw new ExceptionsEDI(string.Format("{0} {1}", EDIHelperFunctions.Help, ErrorCodes.HSAError2));
                }
            }
        }

        /// <summary>
        /// If Dev is Set to 1 then we will use the Dev Connection string
        /// </summary>
        public bool Development
        {
            get
            {
                int iDev = 0;
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);

                if (reg64 != null)
                {
                    object oDev = reg64.GetValue(EDIHelperFunctions.HSA, EDIHelperFunctions.DEV_NOT_FOUND, RegistryValueOptions.None);
                    RegistryValueKind cRegistryValueKind = reg64.GetValueKind(EDIHelperFunctions.Development);
                    iDev = (int)oDev;
                    if (iDev == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    throw new ExceptionsEDI(string.Format("{0} {1}", EDIHelperFunctions.Help, ErrorCodes.HSAError2));
                }
            }
        }

        /// <summary>
        /// Gets the connection string for the database 
        /// </summary>
        public string GetConnectionString
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                object oConnectionString;
                if (reg64 != null)
                {
                    oConnectionString = reg64.GetValue(EDIHelperFunctions.CONNECTIONSTRINGProd, EDIHelperFunctions.CONNECTION_STRING_NOPT_FOUND, RegistryValueOptions.None);
                    foreach (string item in (string[])oConnectionString)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }


        public string GetReportLocationForBOL
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                object oConnectionString;
                if (reg64 != null)
                {
                    oConnectionString = reg64.GetValue(EDIHelperFunctions.Reportpath, EDIHelperFunctions.CONNECTION_STRING_NOPT_FOUND, RegistryValueOptions.None);
                    foreach (string item in (string[])oConnectionString)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }


        public string GetVisaFile
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                object oConnectionString;
                if (reg64 != null)
                {
                    oConnectionString = reg64.GetValue(EDIHelperFunctions.Visapath, EDIHelperFunctions.CONNECTION_STRING_NOPT_FOUND, RegistryValueOptions.None);
                    foreach (string item in (string[])oConnectionString)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }


        public string GetAmexFile
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                object oConnectionString;
                if (reg64 != null)
                {
                    oConnectionString = reg64.GetValue(EDIHelperFunctions.Amexpath, EDIHelperFunctions.CONNECTION_STRING_NOPT_FOUND, RegistryValueOptions.None);
                    foreach (string item in (string[])oConnectionString)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }


        public string ASNFix
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                object ASNFixError;
                if (reg64 != null)
                {
                    ASNFixError = reg64.GetValue(EDIHelperFunctions.ASNFixPath, 
                                                       EDIHelperFunctions.ASN_FIX_ERROR, 
                                                       RegistryValueOptions.None);
                    foreach (string item in (string[])ASNFixError)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }



        public string GetConnectionStringSSCC
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                object oConnectionString;
                if (reg64 != null)
                {
                    oConnectionString = reg64.GetValue(EDIHelperFunctions.CONNECTIONSTRINGProdSSCC, EDIHelperFunctions.CONNECTION_STRING_NOPT_FOUND, RegistryValueOptions.None);
                    foreach (string item in (string[])oConnectionString)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }

        public string GetReportLocation
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                object oConnectionString;
                if (reg64 != null)
                {
                    oConnectionString = reg64.GetValue(EDIHelperFunctions.ReportLocation, EDIHelperFunctions.REPORT_NOT_FOUND, RegistryValueOptions.None);
                    foreach (string item in (string[])oConnectionString)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }

        #region EDI 850
        /// <summary>
        /// The path for the EDI 850 file that will go into the database 
        /// </summary>
        public string GetInboundPathEDI850
        {
            get
            {
                string sResult = string.Empty;
                RegKeys cEDIInboundPathRegistryKeyInfo = new RegKeys();
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                if (reg64 != null)
                {
                    object oUserName = reg64.GetValue(EDIHelperFunctions.EDIFLOER, EDIHelperFunctions.INBOUND_FOLDER_NOT_FOUND, RegistryValueOptions.None);
                    foreach (string item in (string[])oUserName)
                    {
                        return item;
                    }
                }
                return null;
            }
        }
        #endregion

        #region EDI 856
        /// <summary>
        /// Set the path for ASN's for Visa Master card
        /// </summary>
        public string GetASNPathForVisaMC
        {
            get
            {
                RegKeys cEDIInboundPathRegistryKeyInfo = new RegKeys();
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                if (reg64 != null)
                {
                    object oUserName = reg64.GetValue(EDIHelperFunctions.ASNFOLDERLOCATIONVISAMASTERCARD, EDIHelperFunctions.ASN_FOLDER_NOT_FOUND, RegistryValueOptions.None);
                    cEDIInboundPathRegistryKeyInfo.RegistryKey = string.Format(@"HKEY_LOCAL_MACHINE\SOFTWARE\EDI");
                    foreach (string item in (string[])oUserName)
                    {
                        return item;
                    }
                }
                return string.Empty;
            }
        }
        
        public string LabelPath
        {
            get
            {
                RegKeys cEDIInboundPathRegistryKeyInfo = new RegKeys();
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                if (reg64 != null)
                {
                    object oUserName = reg64.GetValue(EDIHelperFunctions.LabelFile, EDIHelperFunctions.ASN_FOLDER_NOT_FOUND, RegistryValueOptions.None);
                    cEDIInboundPathRegistryKeyInfo.RegistryKey = string.Format(@"HKEY_LOCAL_MACHINE\SOFTWARE\EDI");
                    foreach (string item in (string[])oUserName)
                    {
                        return item;
                    }
                }
                return string.Empty;
            }
        }

        public string DotComPath
        {
            get
            {
                RegKeys cEDIInboundPathRegistryKeyInfo = new RegKeys();
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                if (reg64 != null)
                {
                    object oUserName = reg64.GetValue(EDIHelperFunctions.DotCom , EDIHelperFunctions.ASN_FOLDER_NOT_FOUND, RegistryValueOptions.None);
                    cEDIInboundPathRegistryKeyInfo.RegistryKey = string.Format(@"HKEY_LOCAL_MACHINE\SOFTWARE\EDI");
                    foreach (string item in (string[])oUserName)
                    {
                        return item;
                    }
                }
                return string.Empty;
            }
        }


        /// <summary>
        /// Set the path for ASN's for Amex
        /// </summary>
        public string GetASNPathForAmex
        {
            get
            {
                RegKeys cEDIInboundPathRegistryKeyInfo = new RegKeys();
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                if (reg64 != null)
                {
                    object oUserName = reg64.GetValue(EDIHelperFunctions.ASNFOLDERLOCATIONAMEX, EDIHelperFunctions.ASN_FOLDER_NOT_FOUND, RegistryValueOptions.None);
                    cEDIInboundPathRegistryKeyInfo.RegistryKey = string.Format(@"HKEY_LOCAL_MACHINE\SOFTWARE\EDI");
                    foreach (string item in (string[])oUserName)
                    {
                        return item;
                    }
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Get tem location for temp xml file used for ASN maker 
        /// </summary>
        public string GetASNTempLocation
        {
            get
            {
                string sResult = string.Empty;
                RegKeys cEDIInboundPathRegistryKeyInfo = new RegKeys();
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);
                if (reg64 != null)
                {
                    object oUserName = reg64.GetValue(EDIHelperFunctions.ASNTEMPFOLDERLOCATION, EDIHelperFunctions.ASN_FOLDER_NOT_FOUND, RegistryValueOptions.None);
                    cEDIInboundPathRegistryKeyInfo.RegistryKey = string.Format(@"HKEY_LOCAL_MACHINE\SOFTWARE\EDI");
                    foreach (string item in (string[])oUserName)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }
        #endregion

        static bool RedirectionCallback(string url)
        {
            // Return true if the URL is an HTTPS URL.
            return url.ToLower().StartsWith("https://");
        }



        public void SendEmail(string Msg, string Subject, bool inuse)
        {
            EmailRecipient cEmailRecipient = GetUsernameAmdPassword();
            ExchangeService service = new ExchangeService();
            SmtpClient client = new SmtpClient();
            NetworkCredential cNetworkCredential = new NetworkCredential(cEmailRecipient.UserName, cEmailRecipient.Password);
            MailMessage cMailMessage = new MailMessage(cEmailRecipient.EmailAddress, "fjackson0407@yahoo.com");
            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = cNetworkCredential;
            client.Host = "mail.validusa.com";
            cMailMessage.Subject = "this is a test email.";
            cMailMessage.Body = "this is my test email body";
            client.Send(cMailMessage);
        }

        public void SendEmail()
        {

            EmailRecipient cEmailRecipient = GetUsernameAmdPassword();
            ExchangeService service = new ExchangeService();
            service.Credentials = new WebCredentials(cEmailRecipient.UserName,
                cEmailRecipient.Password);
            //("https://red003.mail.apac.microsoftonline.com/EWS/Exchange.asmx");
            service.Url = new Uri("https://mail.validusa.com/EWS/Exchange.asmx");
            //I need to handle this better!!!!
            //service.AutodiscoverUrl("ediprocess@validusa.com");
            EmailMessage message = new EmailMessage(service);
            message.Subject = "Subject";

            message.ToRecipients.Add("fjackson0407@yahoo.com");

            message.Body = "Msg";
            message.Send();
            
        }

        public void SendEmail(string Msg, string Subject)
        {

            EmailRecipient cEmailRecipient = GetUsernameAmdPassword();
            ExchangeService service = new ExchangeService();
            service.Credentials = new WebCredentials(cEmailRecipient.UserName, cEmailRecipient.Password);
            //I need to handle this better!!!!
            service.AutodiscoverUrl(cEmailRecipient.EmailAddress, RedirectionCallback);
            EmailMessage message = new EmailMessage(service);
            message.Subject = Subject;
            List<EmailAddress> lisAddress = new List<EmailAddress>();
            foreach (string item in cEmailRecipient.Recipients)
            {
                EmailAddress cEmailAddress = new EmailAddress();
                cEmailAddress.Address = item;
                lisAddress.Add(cEmailAddress);
            }
            message.ToRecipients.AddRange(lisAddress);

            message.Body = Msg;
            message.Send();



        }

        public EmailRecipient GetUsernameAmdPassword()
        {
            EmailRecipient cEmailInfomation = new EmailRecipient();
            RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            RegistryKey reg64 = localMachineRegistry64.OpenSubKey(EDIHelperFunctions.SoftwareNode, false);

            if (reg64 != null)
            {
                object oUserName = reg64.GetValue(EDIHelperFunctions.USERNAME, EDIHelperFunctions.USERNAME_NOT_FOUND, RegistryValueOptions.None);
                foreach (string item in (string[])oUserName)
                {
                    cEmailInfomation.UserName = item;
                    break;
                }

                object oPassword = reg64.GetValue(EDIHelperFunctions.PASSWORD, EDIHelperFunctions.PASSWORD_NOT_FOUND, RegistryValueOptions.None);
                foreach (string item in (string[])oPassword)
                {
                    cEmailInfomation.Password = item;
                    break;
                }

                object oEmail = reg64.GetValue(EDIHelperFunctions.EMAILADDRESS, EDIHelperFunctions.EMAIL_ADDRESS_NOT_FOUND, RegistryValueOptions.None);
                foreach (string item in (string[])oEmail)
                {
                    cEmailInfomation.EmailAddress = item;
                    break;
                }

                object oSMTP = reg64.GetValue(EDIHelperFunctions.SMTP, EDIHelperFunctions.SMTP_NOT_FOUND, RegistryValueOptions.None);
                foreach (string item in (string[])oSMTP)
                {
                    cEmailInfomation.SMTP = item;
                    break;
                }

                object oRecipients = reg64.GetValue(EDIHelperFunctions.RECIPIENTS, EDIHelperFunctions.RECIPIENTS_NOT_FOUNND, RegistryValueOptions.None);
                string Recipients = string.Empty;
                foreach (string item in (string[])oRecipients)
                {
                    Recipients = item;
                    break;
                }

                string[] results = Recipients.Split(',');
                foreach (string item in results)
                {
                    cEmailInfomation.Recipients.Add(item);
                }

            }

            return cEmailInfomation;

        }

    }
}
