﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using static Common.ValidConstants;
namespace Common.Utility
{
    public class RegistryKeySettings
    {
        /// <summary>
        ///  The connection string for the database 
        /// </summary>
        public string ConnectionString
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(SoftwareNode, false);
                object oConnectionString;
                if (reg64 != null)
                {
                    oConnectionString = reg64.GetValue(ValidConstants.ConnectionString, CONNECTION_STRING_NOPT_FOUND, RegistryValueOptions.None);
                    foreach (string item in (string[])oConnectionString)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }


        public string ReportLocationForBOL
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(SoftwareNode, false);
                object oConnectionString;
                if (reg64 != null)
                {
                    oConnectionString = reg64.GetValue(Reportpath, CONNECTION_STRING_NOPT_FOUND, RegistryValueOptions.None);
                    foreach (string item in (string[])oConnectionString)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }


        public string VisaFile
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(SoftwareNode, false);
                object oConnectionString;
                if (reg64 != null)
                {
                    oConnectionString = reg64.GetValue(Visapath, "", RegistryValueOptions.None);
                    foreach (string item in (string[])oConnectionString)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }


        public string AmexFile
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(SoftwareNode, false);
                object oConnectionString;
                if (reg64 != null)
                {
                    oConnectionString = reg64.GetValue(Amexpath, "", RegistryValueOptions.None);
                    foreach (string item in (string[])oConnectionString)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }
        
        public string ReportLocation
        {
            get
            {
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                RegistryKey reg64 = localMachineRegistry64.OpenSubKey(SoftwareNode, false);
                object oConnectionString;
                if (reg64 != null)
                {
                    oConnectionString = reg64.GetValue(ValidConstants.ReportLocation, "", RegistryValueOptions.None);
                    foreach (string item in (string[])oConnectionString)
                    {
                        return item;
                    }

                }
                return string.Empty;
            }
        }
    }
}
