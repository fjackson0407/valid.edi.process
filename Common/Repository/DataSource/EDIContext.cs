﻿using Domain;
using Repository.Migrations;
using System.Data.Entity;

namespace Repository.DataSource
{
    public class EDIContext : DbContext
    {
        public EDIContext()
        {
        }

        public EDIContext(string ConnectionString)
            : base(ConnectionString)
        {

            Database.SetInitializer<EDIContext>(new DropCreateDatabaseIfModelChanges<EDIContext>());

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EDIContext, Configuration>());
        }

        public virtual DbSet<ShippingAddressInfoDTO> AddressInfo { get; set; }
        public virtual DbSet<PackInfoDTO> PackInfo { get; set; }
        public virtual DbSet<ASNFile856DTO> ASN { get; set; }
        public virtual DbSet<Order850DTO> Order { get; set; }
        public virtual DbSet<CartonInfoDTO> CartonDetail { get; set; }
        public virtual DbSet<BOL856DTO> BOL { get; set; }
        public virtual DbSet<ASNOrderDetail856DTO> ASNOrderDetail { get; set; }
        public virtual DbSet<UserInfoDTO> UserInfo { get; set; }
        public virtual DbSet<OrderDetail850DTO> OrderDetail { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Order850DTO>()
                .HasMany(c => c.OrderDetail)
                .WithOptional(c => c.Order850)
                .HasForeignKey(c => c.Order850_FK)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Order850DTO>()
                .HasMany(c => c.ASN)
                .WithOptional(c => c.Order850)
                .HasForeignKey(c => c.Order850_FK)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Order850DTO>()
                .HasMany(c => c.BOL)
                .WithOptional(c => c.Order850)
                .HasForeignKey(c => c.Order850_FK)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Carton856DTO>()
                .HasMany(c => c.SerialNumbers)
                .WithOptional(c => c.Carton856)
                .HasForeignKey(c => c.Carton856_FK)
                .WillCascadeOnDelete();

            //modelBuilder.Entity<ASNOrderDetail856DTO>()
            //    .HasMany(c => c.SerialNumberInfo  )
            //    .WithOptional(c => c.ASNOrderDetail856 )
            //    .HasForeignKey(c => c.ASNOrderDetail856_FK )
            //    .WillCascadeOnDelete();

            modelBuilder.Entity<ASNOrderDetail856DTO>();

        }
    }
}
