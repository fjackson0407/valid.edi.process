namespace Repository.Migrations
{
    using Domain;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Repository.DataSource.EDIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            
            
        }

        protected override void Seed(Repository.DataSource.EDIContext context)
        {
         IList<CartonInfoDTO> CartonDetail = new List<CartonInfoDTO>();

            CartonDetail.Add(new CartonInfoDTO()
            {
                ID = Guid.NewGuid(),
                Author = "Mel",
                CardCountMin = 25,
                CardCountMax = 100,
                DTS = DateTime.Now,
                Size = "14 X 5.25 X 4",
                SKU = "8301005"

            });


            CartonDetail.Add(new CartonInfoDTO()
            {
                ID = Guid.NewGuid(),
                Author = "Mel",
                CardCountMin = 125,
                CardCountMax = 300,
                DTS = DateTime.Now,
                Size = "11.25 X 8.75 X 9",
                SKU = "8301001.07"

            });
            CartonDetail.Add(new CartonInfoDTO()
            {
                ID = Guid.NewGuid(),
                Author = "Mel",
                CardCountMin = 325,
                CardCountMax = 450,
                DTS = DateTime.Now,
                Size = "11.25 X 8.75 X 12",
                SKU = "8301001.12"

            });

            CartonDetail.Add(new CartonInfoDTO()
            {
                ID = Guid.NewGuid(),
                Author = "Mel",
                CardCountMin = 475,
                CardCountMax = 900,
                DTS = DateTime.Now,
                Size = "11.25 X 8.75 X 12",
                SKU = "8301007.05"

            });

            CartonDetail.Add(new CartonInfoDTO()
            {
                ID = Guid.NewGuid(),
                Author = "Mel",
                CardCountMin = 925,
                CardCountMax = 2000,
                DTS = DateTime.Now,
                Size = "24 X 18 X 12",
                SKU = "8301007.06"

            });

            foreach (var item in CartonDetail)
            {
                context.CartonDetail.Add(item);
            }
            base.Seed(context);
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }

    }
    
}
