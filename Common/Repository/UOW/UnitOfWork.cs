﻿using Repository.DataSource;
using Repository.Implementation;
using Repository.Interfaces;

namespace Repository.UOW
{
  public  class UnitOfWork : IUnitOfWork
    {
        protected string ConnectionString;
        private readonly EDIContext _EDIContext;

        public IASN ASN { get; private set; }
        public IOrders Order { get; private set;  }
        public IOrderDetail OrderDetail { get; private set; }
        public ICartonInfo CartonSize { get; private set;  }
        public ICarton Carton { get; private set;  }
        public IASNOrderDetail ASNOrderDetail { get; private set; }
        public IPackWeight PackWeight { get; private set;  }
        public IShippingAddress ShippingAddress { get; private set;  }
        public IUser User { get; private set; }
        
        public UnitOfWork(EDIContext cEDIContext)
        {
            _EDIContext = cEDIContext;
            Order = new Orders(_EDIContext);
            ASN = new ASN(_EDIContext);
            CartonSize = new CartonInfo(_EDIContext);
            ASNOrderDetail = new ASNOrderDetail(_EDIContext);
            OrderDetail = new OrderDetail(_EDIContext);
            Carton = new Carton(_EDIContext);
            PackWeight = new PackWeight(_EDIContext);
            ShippingAddress = new ShippingAddress(_EDIContext);
            User = new User(_EDIContext);
        }

        public int Complete()
        {
            return _EDIContext.SaveChanges();
        }
        
        public void Dispose()
        {
            _EDIContext.Dispose();
        }
    }
}
