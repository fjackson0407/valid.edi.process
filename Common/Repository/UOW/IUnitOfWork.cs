﻿using Repository.Interfaces;
using System;

namespace Repository.UOW
{
   public  interface IUnitOfWork : IDisposable
    {
        IASN ASN { get;  }
        ICarton Carton { get; }
        IOrderDetail OrderDetail { get;  }
        IOrders Order { get; }
        ICartonInfo CartonSize { get; }
        IASNOrderDetail ASNOrderDetail { get;  }
        IPackWeight PackWeight { get;  }
        IShippingAddress ShippingAddress { get;  }
        IUser User { get;  }
        int Complete();
    }
}
