﻿using Domain;
using Repository.BaseClass;
using Repository.DataSource;
using Repository.Interfaces;

namespace Repository.Implementation
{
  public   class OrderDetail : RepositoryBase<OrderDetail850DTO> , IOrderDetail
    {
        public OrderDetail(EDIContext context)
            : base(context) { }
    }
}
