﻿using Domain;
using Repository.BaseClass;
using Repository.DataSource;
using Repository.Interfaces;

namespace Repository.Implementation
{
  public   class Orders : RepositoryBase<Order850DTO>, IOrders
    {
        public Orders(EDIContext context)
            : base(context) 
        { }

        public int SaveChange()
        {
            return this.Context.SaveChanges();
        }
    }
}
