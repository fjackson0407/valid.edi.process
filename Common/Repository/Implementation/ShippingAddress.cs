﻿using Domain;
using Repository.BaseClass;
using Repository.DataSource;
using Repository.Interfaces;

namespace Repository.Implementation
{
    public class ShippingAddress : RepositoryBase<ShippingAddressInfoDTO>, IShippingAddress
    {
        public ShippingAddress(EDIContext context)
            :base(context ) { }
    }
}
