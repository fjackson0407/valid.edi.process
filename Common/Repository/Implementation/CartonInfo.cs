﻿using Domain;
using Repository.BaseClass;
using Repository.DataSource;
using Repository.Interfaces;

namespace Repository.Implementation
{
   public  class CartonInfo : RepositoryBase<CartonInfoDTO>, ICartonInfo
    {
            public CartonInfo(EDIContext context)
            : base(context) { }
    }
}
