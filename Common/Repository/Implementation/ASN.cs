﻿using Domain;
using Repository.BaseClass;
using Repository.DataSource;
using Repository.Interfaces;

namespace Repository.Implementation
{
public   class ASN : RepositoryBase<ASNFile856DTO>, IASN
    {

        public ASN(EDIContext context)
            : base(context) 
        { }

    }
}
