﻿using Domain;
using Repository.BaseClass;
using Repository.DataSource;
using Repository.Interfaces;

namespace Repository.Implementation
{
public   class PackWeight : RepositoryBase<PackInfoDTO>, IPackWeight
    {
        public PackWeight(EDIContext context)
            : base(context) { }

    }
}
