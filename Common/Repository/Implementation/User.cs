﻿using Domain;
using Repository.BaseClass;
using Repository.DataSource;
using Repository.Interfaces;

namespace Repository.Implementation
{
    public class User : RepositoryBase<UserInfoDTO>, IUser
    {
        public User(EDIContext Context)
            : base(Context) { }
    }
}
