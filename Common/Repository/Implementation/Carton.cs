﻿using Domain;
using Repository.BaseClass;
using Repository.DataSource;
using Repository.Interfaces;
using System;
using System.Linq;
using static Common.ValidConstants;

namespace Repository.Implementation
{
 public    class Carton : RepositoryBase<Carton856DTO> , ICarton
    {
        public Carton(EDIContext context)
        : base(context) { }

        public string CreateNextBarcode()
        {
            var nextSeqNumber = GetNextSeqNumber();
            if (nextSeqNumber != null)
            {
                var value = BuildBarcodeValue(EXTENSIONDIGIT, COMPANYCODE, nextSeqNumber.ToString());
                var barcode = APPLICATIONIDENTIFIER + value + calculateChecksum(value);
                return barcode;
            }
            throw new NullReferenceException("The method GetNextSeqNumber() returned a null value");
        }

         public int? GetNextSeqNumber()
         {
            var lastRecordBySeqNumber = this.GetAll().OrderByDescending(x => x.BarcodeSeqNumber).FirstOrDefault();
             if (lastRecordBySeqNumber != null)
             {
                 return lastRecordBySeqNumber.BarcodeSeqNumber++;
             }
             return null;
         }

        private string BuildBarcodeValue(string ExtenstionDigit, string CompanyCode, string SequenceNumber)
        {
            string sResult = string.Empty;
            const string ZERO = "0";

            if (string.IsNullOrEmpty(ExtenstionDigit) || string.IsNullOrEmpty(CompanyCode) || string.IsNullOrEmpty(SequenceNumber))
            {
                sResult = string.Empty; //Throw error 
            }
            else
            {
                while (ExtenstionDigit.Length + CompanyCode.Length + SequenceNumber.Length != SSCCLENGTH)
                {
                    SequenceNumber = ZERO + SequenceNumber;
                }
                sResult = ExtenstionDigit + CompanyCode + SequenceNumber;
            }
            return sResult;
        }

        private string calculateChecksum(string barcode)
        {
            int even = 0;
            int odd = 0;
            int position = 1;
            int total = 0;
            int delta = 0;

            if (barcode.Length == SSCCLENGTH)
            {
                foreach (char item in barcode)
                {
                    if ((position % 2) == 0)
                    {
                        even += int.Parse(item.ToString());
                    }
                    else
                    {
                        odd += int.Parse(item.ToString());
                    }
                    position++;
                }
                odd *= 3;
                total = even + odd;

                while ((total % 10) != 0)
                {
                    total++;
                    delta++;
                }
            }
            return delta.ToString();
        }

    }
}
