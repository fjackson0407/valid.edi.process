﻿using Domain;
using Repository.BaseClass;
using Repository.DataSource;
using Repository.Interfaces;

namespace Repository.Implementation
{
 public    class ASNOrderDetail : RepositoryBase<ASNOrderDetail856DTO>, IASNOrderDetail
    {

        public ASNOrderDetail(EDIContext context)
            :base(context ) { }
    }
}
