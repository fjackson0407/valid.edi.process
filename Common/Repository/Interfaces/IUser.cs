﻿using Domain;
using Repository.BaseClass;

namespace Repository.Interfaces
{
    public interface IUser : IRepositoryBase<UserInfoDTO>
    {
    }
}
