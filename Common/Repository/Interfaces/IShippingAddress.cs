﻿using Domain;
using Repository.BaseClass;

namespace Repository.Interfaces
{
    public interface IShippingAddress : IRepositoryBase<ShippingAddressInfoDTO>
    {
    }
}
