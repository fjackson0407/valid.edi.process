﻿using Domain;
using Repository.BaseClass;

namespace Repository.Interfaces
{
 public  interface IOrders : IRepositoryBase<Order850DTO>
    {
        int SaveChange();
    }
}
