﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Exceptions
{
    public class EDI850Exceptions : ApplicationException
    {
        public EDI850Exceptions()
        { }
        public EDI850Exceptions(string Message)
            : base(Message) { }
        
    }
}
