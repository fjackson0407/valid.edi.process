﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Win32;

namespace Common
{
    public class Settings
    {
        private const string _registryPath = @"SOFTWARE\EDI";
        private readonly Dictionary<string, string> _settingsDictionary;

        /// <summary>
        /// Constructor
        /// </summary>
        public Settings()
        {
            _settingsDictionary = GetRegistrySettings();
        }

        private Dictionary<string, string> GetRegistrySettings()
        {
            var dictionary = new Dictionary<string, string>();
            using (
                RegistryKey localMachineRegistry64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine,
                    RegistryView.Registry64))
            {
                RegistryKey ediKey = localMachineRegistry64.OpenSubKey(_registryPath, false);
                if (ediKey != null)
                {
                    var nameList = ediKey.GetValueNames().ToList();
                    foreach (var name in nameList)
                    {
                        dictionary.Add(name, ((string[]) ediKey.GetValue(name))[0]);
                    }
                }
            }
            return dictionary;
        }

        /// <summary>
        /// For new database 
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return _settingsDictionary.ContainsKey("ConnectionString")
                    ? _settingsDictionary["ConnectionString"]
                    : string.Empty;
            }
        }

        /// <summary>
        /// This will get the number for ssscc sequence number 
        /// </summary>
        public string ConnectionStringForSSCC
        {
            get { return ConnectionStringOld; }
        }


        public string ConnectionStringOld
        {
            get
            {
                return _settingsDictionary.ContainsKey("ConnectionStringOld")
                    ? _settingsDictionary["ConnectionStringOld"]
                    : string.Empty;
            }
        }


        public string TempPath
        {
            get
            {
                return _settingsDictionary.ContainsKey("TempPath")
                    ? _settingsDictionary["TempPath"]
                    : string.Empty;
            }
        }


        public string ASNPathVisaMcOldVerison
        {
            get
            {
                return _settingsDictionary.ContainsKey("ASNPathVisaMcOldVerison")
                    ? _settingsDictionary["ASNPathVisaMcOldVerison"]
                    : string.Empty;
            }
        }


        public string FileASNPathVisaMcOldVerison
        {
            get
            {
                return _settingsDictionary.ContainsKey("FileASNPathVisaMcOldVerison")
                    ? _settingsDictionary["FileASNPathVisaMcOldVerison"]
                    : string.Empty;
            }
        }


        public string ASNPathAmexOldVerison
        {
            get
            {
                return _settingsDictionary.ContainsKey("ASNPathAmexOldVerison")
                    ? _settingsDictionary["ASNPathAmexOldVerison"]
                    : string.Empty;
            }
        }


        public string FileASNPathAmexOldVerison
        {
            get
            {
                return _settingsDictionary.ContainsKey("FileASNPathAmexOldVerison")
                    ? _settingsDictionary["FileASNPathAmexOldVerison"]
                    : string.Empty;
            }
        }


        public string Root
        {
            get
            {
                return _settingsDictionary.ContainsKey("Root")
                    ? _settingsDictionary["Root"]
                    : string.Empty;
            }
        }


        public string ValidAddress
        {
            get
            {
                return _settingsDictionary.ContainsKey(ValidConstants.ValidAddress)
                    ? _settingsDictionary[ValidConstants.ValidAddress]
                    : string.Empty;
            }
        }

        public string ReportFolder
        {
            get
            {
                return _settingsDictionary.ContainsKey(ValidConstants.ReportFolder)
                    ? _settingsDictionary[ValidConstants.ReportFolder]
                    : string.Empty;
            }
        }

        public string Shipdate
        {
            get
            {
                return _settingsDictionary.ContainsKey(ValidConstants.Shipdate)
                    ? _settingsDictionary[ValidConstants.Shipdate]
                    : DateTime.Now.ToString(ValidConstants.toFormat);
            }
        }

        public string CartonTypes
        {
            get
            {
                return _settingsDictionary.ContainsKey(ValidConstants.CartonTypes)
                    ? _settingsDictionary[ValidConstants.CartonTypes]
                    : string.Empty;
            }
        }

        public string ContactType
        {
            get
            {
                return _settingsDictionary.ContainsKey(ValidConstants.ContactType)
                    ? _settingsDictionary[ValidConstants.ContactType]
                    : DateTime.Now.ToShortDateString();
            }
        }

        public string ASNFolderVisaMaster
        {
            get
            {
                return _settingsDictionary.ContainsKey("ASNFolderVisaMasterCard")
                    ? _settingsDictionary["ASNFolderVisaMasterCard"]
                    : string.Empty;
            }
        }

        public string ASNFolderAmex
        {
            get
            {
                return _settingsDictionary.ContainsKey("ASNFolderAmex")
                    ? _settingsDictionary["ASNFolderAmex"]
                    : string.Empty;
            }
        }

        public string ASNFolderVisaMasterCardOld
        {
            get
            {
                return _settingsDictionary.ContainsKey("ASNFolderVisaMasterCardOld")
                    ? _settingsDictionary["ASNFolderVisaMasterCardOld"]
                    : string.Empty;
            }
        }

        public string ASNFolderAmexOld
        {
            get
            {
                return _settingsDictionary.ContainsKey("ASNFolderAmexOld")
                    ? _settingsDictionary["ASNFolderAmexOld"]
                    : string.Empty;
            }
        }


        public string ASNContactEmailAddress
        {
            get
            {
                return _settingsDictionary.ContainsKey("ASNContactEmailAddress")
                    ? _settingsDictionary["ASNContactEmailAddress"]
                    : string.Empty;
            }
        }


        public string EDI850FolderPath
        {
            get
            {
                return _settingsDictionary.ContainsKey("EDI850FolderPath")
                    ? _settingsDictionary["EDI850FolderPath"]
                    : string.Empty;
            }
        }

        public string CurrentVersion
        {
            get
            {
                Assembly assem = Assembly.GetEntryAssembly();
                AssemblyName assemName = assem.GetName();
                Version ver = assemName.Version;

                return ver.ToString();
            }
        }

        public override string ToString()
        {
            string settings = string.Empty;
            foreach (var data in _settingsDictionary)
            {
                if (data.Key.Contains("Password"))
                {
                    settings = string.Format("{0}, {1} : {2}", settings, data.Key, "******");
                }
                else
                {
                    settings = string.Format("{0}, {1} : {2}", settings, data.Key, data.Value);
                }
            }
            return settings;
        }

        public Dictionary<string, string> ToDictionary()
        {
            return _settingsDictionary;
        }
    }
}