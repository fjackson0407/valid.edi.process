﻿namespace Common.Mappers
{
    public class Mapper
    {
        public enum ASNStatus : int
        {
            ReadyForASN = 1,
            HasASN = 2,
            ErrorASN = 3,
            InCarton = 4,
            Packed = 5,
            RadyForFile = 6
        }

        public enum SSCCStatus : int
        {
            NotUsed = 0,
            Used = 1,
        }
        public enum PickStatus : int
        {
            Open = 0,
            Closed = 1
        }

        public enum ItemStatus : int
        {
            NotInCarton = 0,
            InCarton = 1,
            ItemInCartonAndinASN  = 2 
            
        }
        public enum ShippingInfoType : int
        {
            ShipmentTotalCases = 1,
            ShipmentTotalWeight = 2,
            PickCount = 3,

        }
        public enum Inbound850Mapping : int
        {
            CompanyCode = 0,
            CustomerNumber = 1,
            PONumber = 2,
            LocationNumber = 3,
            DepartmentNo = 5,
            VendorNumber = 6,
            PODate = 7,
            ShipDateOrder = 8,
            CancelDate = 9,
            OrderStoreNumber = 12,
            OrderAmount = 16,
            OrderCases = 17,
            CustomerLine = 19,
            VendorItemNo = 21,
            UPCCode = 22,
            DPCI = 23,
            QtyOrdered = 24,
            UnitofMeasure = 32,
            Numberofboxes = 26,
            UnitPrice = 34,
            ItemDescription = 42,
            BillToAddress = 50,
            ShipToAddress = 51,
            DocumentId = 52,
            OriginalLine = 63,
        }

    }
}
