﻿
using Common;
using Domain;
using EDIException;
using Helpers;
using LumenWorks.Framework.IO.Csv;
using OldRepository.DataSource;
using OldRepository.UOW;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolService;
using static Helpers.EDIHelperFunctions;

namespace EDIServiceV1
{
    public class EDIPOServiceV1
    {
        public string m_sEDIFile { get; set; }
        public string TempPath { get; set; }
        public string ASNPath { get; set; }
        public string labelPath { get; set; }
        public string FileDate { get; set; }

        public int NumberofPackesPerbox { get; private set; }
        public int NumberofLabelesPerBox { get; private set; }
        public Settings m_settings { get; set; }

        #region Constructors
        public EDIPOServiceV1(Settings _Settings, string _FileName)
        {
            m_settings = _Settings;
            m_sEDIFile = _FileName;
        }
        #endregion

        public List<StoreInfoFromEDI850> BuildReport()
        {
            return ParseFileForReport();
        }

        public int ParseEDI850()
        {
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld)))
            {
                List<StoreInfoFromEDI850> Invoice = ParseFile();
                UoW.AddEDI850.AddRage(Invoice);
                return UoW.Complate();

            }
        }


        private List<StoreInfoFromEDI850> ParseFileForReport()
        {

            List<StoreInfoFromEDI850> Invoice = new List<StoreInfoFromEDI850>();
            using (CsvReader csv = new CsvReader(new StreamReader(m_sEDIFile), true, CsvReader.DefaultDelimiter, CsvReader.DefaultQuote, CsvReader.DefaultEscape, CsvReader.DefaultDelimiter, ValueTrimmingOptions.None))
            {
                csv.SupportsMultiline = true;
                IDataReader reader = csv;
                StoreInfoFromEDI850 cEDI850Domain = null;

                string sPO = string.Empty;

                while (reader.Read())
                {
                    cEDI850Domain = new StoreInfoFromEDI850();
                    sPO = reader.GetValue((int)Inbound850Mapping.PONumber).ToString();
                    cEDI850Domain.Id = Guid.NewGuid();
                    cEDI850Domain.PONumber = sPO.Replace(@"'", "");
                    cEDI850Domain.PODate = reader.GetDateTime((int)Inbound850Mapping.PODate);
                    FileDate = cEDI850Domain.PODate.ToShortDateString();
                    cEDI850Domain.DTS = DateTime.Now;
                    cEDI850Domain.ASNStatus = (int)ASNStatus.ReadyForASN;
                    cEDI850Domain.PickStatus = 0;
                    cEDI850Domain.DPCI = reader.GetValue((int)Inbound850Mapping.DPCI).ToString();
                    cEDI850Domain.QtyOrdered = reader.GetInt32((int)Inbound850Mapping.QtyOrdered);
                    cEDI850Domain.UPCode = reader.GetValue((int)Inbound850Mapping.UPCCode).ToString().Replace(@"'", "");
                    cEDI850Domain.CustomerNumber = reader.GetValue((int)Inbound850Mapping.CustomerNumber).ToString();
                    cEDI850Domain.CompanyCode = reader.GetValue((int)Inbound850Mapping.CompanyCode).ToString();
                    if (cEDI850Domain.CompanyCode == "STO08")
                    {
                        cEDI850Domain.PkgWeight = EDIHelperFunctions.AmexWeight;
                    }
                    else
                    {
                        cEDI850Domain.PkgWeight = EDIHelperFunctions.MCVisaWeight;
                    }
                    cEDI850Domain.ShippingLocationNumber = reader.GetValue((int)Inbound850Mapping.LocationNumber).ToString();
                    cEDI850Domain.VendorNumber = reader.GetValue((int)Inbound850Mapping.VendorNumber).ToString();
                    cEDI850Domain.ShipDate = reader.GetDateTime((int)Inbound850Mapping.ShipDateOrder).ToString();
                    cEDI850Domain.CancelDate = reader.GetDateTime((int)Inbound850Mapping.CancelDate).ToString();
                    cEDI850Domain.DCNumber = reader.GetValue((int)Inbound850Mapping.ShipToAddress).ToString().Replace(@"'", "");
                    cEDI850Domain.CancelDate = reader.GetDateTime((int)Inbound850Mapping.CancelDate).ToString();
                    cEDI850Domain.OrderStoreNumber = reader.GetValue((int)Inbound850Mapping.OrderStoreNumber).ToString();
                    cEDI850Domain.BillToAddress = reader.GetValue((int)Inbound850Mapping.BillToAddress).ToString().Replace(@"'", "");
                    cEDI850Domain.DocumentId = reader.GetValue((int)Inbound850Mapping.DocumentId).ToString();
                    cEDI850Domain.OriginalLine = reader.GetValue((int)Inbound850Mapping.OriginalLine).ToString();
                    Invoice.Add(cEDI850Domain);
                }

            }
            return Invoice;
        }


        private List<StoreInfoFromEDI850> ParseFile()
        {

            List<StoreInfoFromEDI850> Invoice = new List<StoreInfoFromEDI850>();
            System.Threading.Thread.Sleep(3000);
            using (CsvReader csv = new CsvReader(new StreamReader(m_sEDIFile), true, CsvReader.DefaultDelimiter, CsvReader.DefaultQuote, CsvReader.DefaultEscape, CsvReader.DefaultDelimiter, ValueTrimmingOptions.None))
            {
                if (csv == null)
                {
                    //Cisco, csv will never be null but the contents may be.
                    throw new ExceptionsEDI(string.Format("{0}  {1}", Help, ErrorCodes.HSAError5));
                }

                csv.SupportsMultiline = true;
                IDataReader reader = csv;
                StoreInfoFromEDI850 cEDI850Domain = null;

                string sPO = string.Empty;

                while (reader.Read())
                {
                    cEDI850Domain = new StoreInfoFromEDI850();
                    sPO = reader.GetValue((int)Inbound850Mapping.PONumber).ToString();
                    cEDI850Domain.Id = Guid.NewGuid();
                    cEDI850Domain.PONumber = sPO.Replace(@"'", "");
                    cEDI850Domain.PODate = reader.GetDateTime((int)Inbound850Mapping.PODate);
                    cEDI850Domain.DTS = DateTime.Now;
                    cEDI850Domain.ASNStatus = (int)ASNStatus.ReadyForASN;
                    cEDI850Domain.PickStatus = 0;
                    cEDI850Domain.QtyOrdered = reader.GetInt32((int)Inbound850Mapping.QtyOrdered);
                    cEDI850Domain.UPCode = reader.GetValue((int)Inbound850Mapping.UPCCode).ToString().Replace(@"'", "");
                    ToolService.CommonFunctions cCommonFunctions = new ToolService.CommonFunctions(m_settings.ConnectionStringOld);
                    SkuItem cSkuItem = cCommonFunctions.GetSkuInfo(cEDI850Domain.UPCode);
                    if (cSkuItem != null)
                    {
                        cEDI850Domain.SkuItemFK = cSkuItem.Id;
                        cEDI850Domain.CustomerNumber = reader.GetValue((int)Inbound850Mapping.CustomerNumber).ToString();
                        cEDI850Domain.CompanyCode = reader.GetValue((int)Inbound850Mapping.CompanyCode).ToString();
                        if (cEDI850Domain.CompanyCode == "STO08")
                        {
                            cEDI850Domain.PkgWeight = EDIHelperFunctions.AmexWeight;
                        }
                        else
                        {
                            cEDI850Domain.PkgWeight = EDIHelperFunctions.MCVisaWeight;
                        }
                        cEDI850Domain.ShippingLocationNumber = reader.GetValue((int)Inbound850Mapping.LocationNumber).ToString();
                        cEDI850Domain.VendorNumber = reader.GetValue((int)Inbound850Mapping.VendorNumber).ToString();
                        cEDI850Domain.ShipDate = reader.GetDateTime((int)Inbound850Mapping.ShipDateOrder).ToString();
                        cEDI850Domain.CancelDate = reader.GetDateTime((int)Inbound850Mapping.CancelDate).ToString();
                        cEDI850Domain.DCNumber = reader.GetValue((int)Inbound850Mapping.ShipToAddress).ToString().Replace(@"'", "");
                        cEDI850Domain.CancelDate = reader.GetDateTime((int)Inbound850Mapping.CancelDate).ToString();
                        cEDI850Domain.OrderStoreNumber = reader.GetValue((int)Inbound850Mapping.OrderStoreNumber).ToString();
                        cEDI850Domain.BillToAddress = reader.GetValue((int)Inbound850Mapping.BillToAddress).ToString().Replace(@"'", "");
                        cEDI850Domain.DocumentId = reader.GetValue((int)Inbound850Mapping.DocumentId).ToString();
                        cEDI850Domain.OriginalLine = reader.GetValue((int)Inbound850Mapping.OriginalLine).ToString();
                        Invoice.Add(cEDI850Domain);
                    }

                }

            }
            return Invoice;
        }

    }
}
