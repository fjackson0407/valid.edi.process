﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.ASNCartonTypes
{
    public class CartonTypeForASN: RepositoryBase<CartonType>, ICartonTypeForASN 
    {

        public CartonTypeForASN(OldEDIContext context)
            :base(context )
        { }

        public int SaveChange()
        {
            return this.Context.SaveChanges();
        }


    }
}
