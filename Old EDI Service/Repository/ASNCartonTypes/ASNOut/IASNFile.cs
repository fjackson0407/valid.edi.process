﻿using Domain;
using OldRepository.BaseClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.ASNOut
{
    public interface  IASNFile : IRepositoryBase<ASNFileOutBound>
    {
        int SaveChanges();
    }
}
