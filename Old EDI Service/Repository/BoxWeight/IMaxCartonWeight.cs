﻿using Domain;
using OldRepository.BaseClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.BoxWeight
{
    public interface  IMaxCartonWeight : IRepositoryBase<MaxWeightFullBox >
    {
        int SaveChanges();
    }
}
