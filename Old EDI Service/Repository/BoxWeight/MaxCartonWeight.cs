﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.BoxWeight
{
   public  class MaxCartonWeight : RepositoryBase<MaxWeightFullBox> , IMaxCartonWeight
    {

        public MaxCartonWeight(OldEDIContext context)
            : base(context) { }


        public int SaveChanges()
        {
            return this.Context.SaveChanges();
        }
    }
}
