﻿using OldRepository.ASNCartonTypes;
using OldRepository.ASNOut;
using OldRepository.Barcode;
using OldRepository.BillOfLadingFolder;
using OldRepository.BoxWeight;
using OldRepository.BundleWeightForCardType;
using OldRepository.Cartons;
using OldRepository.ContactData856;
using OldRepository.DC;
using OldRepository.EmptyBoxFolder;
using OldRepository.Inbound850;
using OldRepository.MinWeightFolder;
using OldRepository.Notes;
using OldRepository.OperatorFolder;
using OldRepository.PackSize;
using OldRepository.SerialRageNumberFolder;
using OldRepository.Settings;
using OldRepository.Shipping;
using OldRepository.ShipProduct;
using OldRepository.SkuFolder;
using OldRepository.UserOrderFolder;
using OldRepository.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.UOW
{
    public interface IUnitofWork : IDisposable
    {

        IAddEDI850 AddEDI850 { get; }
        ISkus Sku { get; }
        IBOL Bol { get; }
        ISSCCBarcode SSCCBarcode { get; }
        ISerialRage SerialRage { get; }
        IDCInfo DCInfo { get; }
        IShipFrom ShipFrom {get;  }
        IASNContact ASNContact { get; }
        IOperator Operator { get; }
        ICartons856 _Cartons { get;  }
        IUserLogin User { get; }
        IUserOrderLog UserOrderLog { get; }
        IASNFile ASNFile { get; }
        IMaxCartonWeight MaxCartonWeight { get;  }
        ICartonTypeForASN CartonType { get; }
        IPackSizeForBundles PackSizeForBundles { get; }
        IShipDateRequest ShipDateRequest { get; }
        ICardWeight CardWeight { get; }
        IEmptyBox EmptyBox { get; }
        IMinWeightShipBox MinWeightShipBox { get; }
        IStoreNotesRepo StoreNotesRepo { get; }
        ISettingsASNOutbound SettingsASNOutbound { get; }
        int Complate();
    }
}
 