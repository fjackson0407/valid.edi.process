﻿using OldRepository.ASNCartonTypes;
using OldRepository.ASNOut;
using OldRepository.Barcode;
using OldRepository.BillOfLadingFolder;
using OldRepository.BoxWeight;
using OldRepository.BundleWeightForCardType;
using OldRepository.Cartons;
using OldRepository.ContactData856;
using OldRepository.DataSource;
using OldRepository.DC;
using OldRepository.EmptyBoxFolder;
using OldRepository.Inbound850;
using OldRepository.MinWeightFolder;
using OldRepository.Notes;
using OldRepository.OperatorFolder;
using OldRepository.PackSize;
using OldRepository.SerialRageNumberFolder;
using OldRepository.Settings;
using OldRepository.Shipping;
using OldRepository.ShipProduct;
using OldRepository.SkuFolder;
using OldRepository.UserOrderFolder;
using OldRepository.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.UOW
{
    public class OldUnitofWork : IUnitofWork
    {
        protected string ConnectionString;
        private readonly OldEDIContext _EDIContext;
        public IAddEDI850 AddEDI850 { get; private set; }
        
        public ISkus Sku { get; private set;  }
        public ISSCCBarcode SSCCBarcode { get; private set; }

        public IDCInfo DCInfo { get; private set;  }

        public  IShipFrom ShipFrom { get; private set; }
        public IASNContact ASNContact { get; private set; }

        public IOperator Operator { get; private set; }
        public ISerialRage SerialRage { get; private set; }
        public IBOL Bol { get; private set;  }

        public IUserLogin User { get; private set;  }

        public IUserOrderLog UserOrderLog { get; private set; }

        public IMaxCartonWeight MaxCartonWeight { get; private set;}

        public IPackSizeForBundles PackSizeForBundles { get; private set;  }
        public ICartons856 _Cartons { get; private set; }
        public ICartonTypeForASN CartonType { get; private set;  }

        public IASNFile ASNFile { get; private set; }

        public IShipDateRequest ShipDateRequest { get; private set; }

         public  ICardWeight CardWeight { get; private set;  }
        public IEmptyBox EmptyBox { get; private set; }

        public  IMinWeightShipBox MinWeightShipBox { get; private set;  }
        
        public IStoreNotesRepo StoreNotesRepo { get; private set; }

        public ISettingsASNOutbound SettingsASNOutbound { get; private set;  }
       public OldUnitofWork(OldEDIContext cEDIContext)
        {

            _EDIContext = cEDIContext;
           AddEDI850 = new AddEDI850(_EDIContext);
            SettingsASNOutbound = new SettingsASNOutbound(_EDIContext);
            User = new UserLogin(_EDIContext);
            Sku = new Skus(_EDIContext);
            SSCCBarcode = new SSCCBarcode(_EDIContext);
            DCInfo = new DCInfo(_EDIContext);
            ShipFrom = new ShipFrom(_EDIContext);
            ASNContact = new ASNContact(_EDIContext);
            _Cartons = new Cartons856(_EDIContext);
            Operator = new Operator(_EDIContext);
            Bol = new BOL(_EDIContext);
            UserOrderLog = new UserOrderLog(_EDIContext);
            MaxCartonWeight = new MaxCartonWeight(_EDIContext);
            PackSizeForBundles = new PackSizeForBundles(_EDIContext);
            CartonType = new CartonTypeForASN(_EDIContext);
            ASNFile = new ASNFile(_EDIContext);
            ShipDateRequest = new ShipDateRequest(_EDIContext);
            CardWeight = new CardWeight(_EDIContext);
            EmptyBox = new EmptyBox(_EDIContext);
            MinWeightShipBox = new MinWeightShipBox(_EDIContext);
            StoreNotesRepo = new StoreNotesRepo(_EDIContext);
           
        }

        /// <summary>
        /// This function will save the data to theee 
        /// </summary>
        /// <returns></returns>
        public int Complate()
        {
            return _EDIContext.SaveChanges();

        }

        public void Dispose()
        {
            _EDIContext.Dispose();
        }

    }
}
