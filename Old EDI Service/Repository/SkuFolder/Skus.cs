﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.SkuFolder
{
   public  class Skus : RepositoryBase<SkuItem>, ISkus 
    {
        public Skus(OldEDIContext context)
            :base(context )

        {

        }
    }
}
