﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.Settings
{
 public    class SettingsASNOutbound : RepositoryBase<ASNSetting856>, ISettingsASNOutbound
    {
        public SettingsASNOutbound(OldEDIContext context)
            : base(context)
        { }
    }
}
