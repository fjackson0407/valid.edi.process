﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.Shipping
{
    public class ShipFrom: RepositoryBase<ShipFromInformation> , IShipFrom 
    {

        public ShipFrom(OldEDIContext context)
            : base(context)
        { }
    }
}
