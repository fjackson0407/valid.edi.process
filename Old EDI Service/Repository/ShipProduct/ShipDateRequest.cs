﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.ShipProduct
{
  public  class ShipDateRequest : RepositoryBase<ShipDate>, IShipDateRequest 
    {
        public ShipDateRequest(OldEDIContext context)
            : base(context) { }
        
        public int SaveChange()
        {
            return this.Context.SaveChanges();
        }
    }
}
