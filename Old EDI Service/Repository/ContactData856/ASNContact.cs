﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.ContactData856
{
    public class ASNContact: RepositoryBase<ContactType> , IASNContact 
    {
        public ASNContact(OldEDIContext context)
              :base(context )
        {

        }
    }
}
