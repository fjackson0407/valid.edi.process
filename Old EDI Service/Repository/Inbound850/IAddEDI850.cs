﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.Inbound850;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.Inbound850
{
    public interface IAddEDI850: IRepositoryBase<StoreInfoFromEDI850>
    {
        
        int SaveChange();
    }
}
