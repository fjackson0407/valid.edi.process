﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.Inbound850
{
  public  class AddEDI850: RepositoryBase<StoreInfoFromEDI850>, IAddEDI850
    {

        public AddEDI850(OldEDIContext context)
            : base(context) 
        { }

        public int SaveChange()
        {
            return this.Context.SaveChanges();
        }
     
    }
}
