﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.SerialRageNumberFolder
{
    public  class SerialRage :RepositoryBase<SerialRageNumber >, ISerialRage 
    {

        public SerialRage(OldEDIContext context )
            :base(context  ) { }

        public int SaveChanges()
        {
            return this.Context.SaveChanges();
        }
    }
}
