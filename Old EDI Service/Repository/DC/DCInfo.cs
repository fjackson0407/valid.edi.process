﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.DC
{
    public class  DCInfo : RepositoryBase<DCInformation>,  IDCInfo 
    {
        public DCInfo(OldEDIContext context )
            :base(context )
        { }

    }
}
