﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.Notes
{
    public class StoreNotesRepo : RepositoryBase<StoreNotes>, IStoreNotesRepo
    {
        public StoreNotesRepo(OldEDIContext context)
            : base(context) 
        { }


    }
}
