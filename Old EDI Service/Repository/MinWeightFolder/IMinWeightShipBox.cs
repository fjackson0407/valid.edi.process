﻿using Domain;
using OldRepository.BaseClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.MinWeightFolder
{
   public  interface IMinWeightShipBox : IRepositoryBase<MinWeightForShipping>
    {
        int SaveChange();
    }
}
