﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.ASNSettings
{
 public     class ASN856Settings : RepositoryBase<ASNConfig> , IASN856Settings
    {

        public ASN856Settings(OldEDIContext context)
            : base(context)
        { }

    }
}
