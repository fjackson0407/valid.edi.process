﻿using Domain;
using OldRepository.BaseClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.UserOrderFolder
{
   public  interface IUserOrderLog : IRepositoryBase<UserOrders>
    {
        int SaveChange();
    }
}
