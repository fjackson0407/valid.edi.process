﻿using Domain;
using OldRepository.BaseClass;
using OldRepository.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRepository.UserOrderFolder
{
  public   class UserOrderLog : RepositoryBase<UserOrders>, IUserOrderLog 
    {
        public UserOrderLog(OldEDIContext context)
            : base(context) { }

        public int SaveChange()
        {
            return this.Context.SaveChanges();
        }

    }
}
