﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
public   class ASNSet
    {
       public string  CartonTypes { get; set; }
       public  DateTime  ShipDate { get; set; }
       public int    Packs { get; set; }
       public int  MaxWeightFullBoxes { get; set; }
        public int EmptyBoxWeights { get; set; }
       public int  MinWeightForShippings { get; set; }
        public ShipFromInformation ShipFrom { get; set;  }

    }
}
