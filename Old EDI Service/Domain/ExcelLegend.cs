﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
  public class ExcelLegend
    {
        public string  Position1 { get; private  set; }
        public string Position2 { get; private set; }
        public string Position3 { get; private set; }
        public string Position4 { get; private set; }
        public string Position5 { get; private set; }
        public string Position6 { get; private set; }
        public string Position7 { get; private set; }


    }
}
