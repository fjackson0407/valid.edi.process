﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
 public   class ASNSetting856
    {
        public Guid  Id { get; set; }
        public string  Name { get; set; }
        public string  Value { get; set; }
        public DateTime DTS { get; set; }
        public bool  Status { get; set; }
    }
}
