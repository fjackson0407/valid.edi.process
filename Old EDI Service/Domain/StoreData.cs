﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Helpers.EDIHelperFunctions;

namespace Domain
{
    public class StoreData
    {
        public int  Store { get; set; }
        public string PO { get; set; }
        public string UPC { get; set; }
        public string DPCI { get; set; }
        public RowColor Color { get; set; }
        public string DPCILastFour
        {
            get
            { return DPCI.Substring(DPCI.Length - 4, 4); }
        }

        public int QtyOrdered { get; set; }
        public int Pkgs
        {
            get
            { return QtyOrdered / 25; }
        }
    }
}
