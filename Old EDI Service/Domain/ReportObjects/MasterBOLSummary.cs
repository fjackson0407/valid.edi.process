﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ReportObjects
{
  public   class MasterBOLSummary
    {
        public string  PONumber { get; set; }
        public int Boxes { get; set; }
        public double  Pounds { get; set; }
    }
}
