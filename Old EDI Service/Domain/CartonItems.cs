﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
public   class CartonItems
    {
        public string UPC { get; set; }
        public string dpci { get; set; }
        public string  Store { get; set; }
        public string  DC { get; set; }
    }
}
