﻿using Common;
using Domain;
using EDIException;
using Helpers;
using OldRepository.DataSource;
using OldRepository.Shipping;
using OldRepository.UOW;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

using static Helpers.EDIHelperFunctions;
using static ToolService.UpdateDatabas;

namespace ASNService
{
    /// <summary>
    /// This code will be used for the master console app 
    /// </summary>
    public class ASNBuild
    {
        #region Class members
        public string m_sPO { get; set; }
        public string m_sCurrentDCNumber { get; set; }
        public string m_sCurrentStore { get; set; }
        public string m_sCompanyCode { get; private set; }

        public string m_sPathForASNFile { get; set; }
        private List<Label> m_lislisLables { get; set; }
        private ShipFromInformation m_cShipFromInformation { get; set; }
        public ASNSet m_cASNSet { get; set; }
 
        public Settings m_settings  { get; set; }
        #endregion
        #region Constructors

        public ASNBuild(Settings _Settings )
        {
            m_settings = _Settings;
            m_cASNSet = new ASNSet();
        }
         #endregion

        public void BuildSpecial(IEnumerable<SpecialOrder> Orders)
        {
            SpecialOrder cSpecialOrder = new SpecialOrder();
            XElement File = new XElement(FILE,
           new XElement(EDIHelperFunctions.DOCUMENT,
                        BuildHeader(Orders.Take(1).FirstOrDefault()),
                        BuildShipmentLevel(Orders),
                        BuildOrderLevel(Orders)));
            SaveDataToFile(File, Orders.Take(1).FirstOrDefault());
            m_lislisLables.Add(AddNewLabel(File));
            //SaveLablesToCSVFile(Orders.FirstOrDefault().NumberofLabelesPerBox);
        }

        /// <summary>
        /// Process all orders that do not have a ASN 
        /// </summary>
        public void ProcessASN()
        {
            
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld)))
            {
                SetASNValues();
                for (;;)
                {
                    StoreInfoFromEDI850 cStore = UoW.AddEDI850.Find(t => t.ASNStatus == (int)ASNStatus.ReadyForASN).FirstOrDefault();
                    if (cStore == null)
                    {
                        break;
                    }
                    m_sPO = cStore.PONumber;
                    m_sCurrentDCNumber = cStore.DCNumber;
                    m_sCurrentStore = cStore.OrderStoreNumber;
                    SetCompanyCode();
                    m_sPathForASNFile = string.Format("{0}ASN Store {1} for PO {2} {3}.xml", m_settings.ASNPathVisaMcOldVerison , m_sCurrentStore, m_sPO, GetNewShipmentID);
                    BuildASN();
                }
            }
        }
      
        /// <summary>
        /// Set values for ASN
        /// </summary>
        private void SetASNValues()
        {
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld   )))
            {
                    var ddt =  UoW.SettingsASNOutbound.GetAll();
                m_cASNSet.CartonTypes = UoW.SettingsASNOutbound.Find(t => t.Status == true && t.Name == ASNSettingsNames.CartonTypes.ToString()).FirstOrDefault().Value;
                m_cASNSet.EmptyBoxWeights = ConvertToInt(UoW.SettingsASNOutbound.Find(t => t.Status == true && t.Name == ASNSettingsNames.EmptyBoxWeights.ToString()).FirstOrDefault().Value);
                m_cASNSet.MaxWeightFullBoxes = ConvertToInt(UoW.SettingsASNOutbound.Find(t => t.Status == true && t.Name == ASNSettingsNames.MaxWeightFullBoxes.ToString()).FirstOrDefault().Value);
                m_cASNSet.MinWeightForShippings = ConvertToInt(UoW.SettingsASNOutbound.Find(t => t.Status == true && t.Name == ASNSettingsNames.MinWeightForShippings.ToString()).FirstOrDefault().Value);
                m_cASNSet.Packs = ConvertToInt(UoW.SettingsASNOutbound.Find(t => t.Status == true && t.Name == ASNSettingsNames.Packs.ToString()).FirstOrDefault().Value);
                m_cASNSet.ShipDate = Convert.ToDateTime(UoW.SettingsASNOutbound.Find(t => t.Status == true && t.Name == ASNSettingsNames.ShipDate.ToString()).FirstOrDefault().Value);


            }
        }

        int ConvertToInt(string Value)
        {
            int result = 0;
            if (int.TryParse(Value, out result))
            {
                return result;
            }
            else
            {
                return 999;
            }
        }


        XElement BuildHeader()
        {
            return new XElement(HEADER,
                new XElement(CompanyCode, m_sCompanyCode),
                new XElement(CUSTOMERNUMBER, GetCustomerNumber()),
                new XElement(Direction, EDIHelperFunctions.Outbound),
                new XElement(DocumentType, EDIHelperFunctions.EDI856),
               new XElement(EDIHelperFunctions.Version, VersionNumber),
              new XElement(EDIHelperFunctions.Footprint, ASN),
              new XElement(ShipmentID, GetNewShipmentID),
             new XElement(EDIHelperFunctions.ShippingLocationNumber, new XCData(m_sCurrentStore)));

        }

        XElement BuildHeader(SpecialOrder cHoildayOrder)
        {
            return new XElement(HEADER,
                new XElement(CompanyCode, cHoildayOrder.CompanyCode),
                new XElement(CUSTOMERNUMBER, cHoildayOrder.CustomerLineNumber),
                new XElement(Direction, EDIHelperFunctions.Outbound),
                new XElement(DocumentType, EDIHelperFunctions.EDI856),
               new XElement(EDIHelperFunctions.Version, VersionNumber),
              new XElement(EDIHelperFunctions.Footprint, ASN),
              new XElement(ShipmentID, GetNewShipmentID),
             new XElement(EDIHelperFunctions.ShippingLocationNumber, new XCData(cHoildayOrder.Store)));

        }


        XElement BuildShipmentLevel(IEnumerable<SpecialOrder> cHoildayOrder)
        {

            return new XElement(EDIHelperFunctions.ShipmentLevel,
       new XElement(TransactionSetPurposeCode, "00"),
                    new XElement(EDIHelperFunctions.DateLoop,
                    new XElement(EDIHelperFunctions.DateQualifier, ShipDateDateQualifierNumber,
                    new XAttribute(EDIHelperFunctions.Desc,
                    EDIHelperFunctions.ShipDateString)),
                    new XElement(EDIHelperFunctions.Date, DateTime.Now.ToString(toFormat))),
                    new XElement(EDIHelperFunctions.ManifestCreateTime,
                     GetMilitaryTime()),
                    new XElement(EDIHelperFunctions.ShipmentTotals,
                    new XElement(EDIHelperFunctions.ShipmentTotalCube, ShipmentTotalCubeValue),
                    new XElement(EDIHelperFunctions.ShipmentPackagingCode, m_cASNSet.CartonTypes),
                    new XElement(ShipmentTotalCases, 1),
                    new XElement(ShipmentTotalWeight, GetTotalWeight(cHoildayOrder))),
                    BuildCarrier(),
                    new XElement(MethodOfPayment, EDIHelperFunctions.DF),
                    new XElement(FOBLocationQualifier, "OR"),
                    new XElement(FOBDescription, new XCData(EDIHelperFunctions.FOBDescription2)),
                    BuildShipFrom(), BuildShipTo(cHoildayOrder.FirstOrDefault()),
                    new XElement(EDIHelperFunctions.VendorCode, cHoildayOrder.FirstOrDefault().VendorNumber),
                    BuildContactType());
        }


        XElement BuildShipmentLevel()
        {

            return new XElement(EDIHelperFunctions.ShipmentLevel,
       new XElement(TransactionSetPurposeCode, "00"),
                    new XElement(EDIHelperFunctions.DateLoop,
                    new XElement(EDIHelperFunctions.DateQualifier, ShipDateDateQualifierNumber,
                    new XAttribute(EDIHelperFunctions.Desc,
                    EDIHelperFunctions.ShipDateString)),
                    new XElement(EDIHelperFunctions.Date, m_cASNSet.ShipDate.ToString(toFormat))),
                    new XElement(EDIHelperFunctions.ManifestCreateTime,
                     GetMilitaryTime()),
                    new XElement(EDIHelperFunctions.ShipmentTotals,
                    new XElement(EDIHelperFunctions.ShipmentTotalCube, ShipmentTotalCubeValue),
                    new XElement(EDIHelperFunctions.ShipmentPackagingCode, m_cASNSet.CartonTypes),
                    new XElement(ShipmentTotalCases, GetShippingWeightOrBoxCount(ShippingInfoType.BoxCount)),
                    new XElement(ShipmentTotalWeight, GetShippingWeightOrBoxCount(ShippingInfoType.ShippingWeight))),
                    BuildCarrier(), BuildBOL(),
                    new XElement(MethodOfPayment, EDIHelperFunctions.DF),
                    new XElement(FOBLocationQualifier, "OR"),
                    new XElement(FOBDescription, new XCData(EDIHelperFunctions.FOBDescription2)),
                    BuildShipFrom(), BuildShipTo(),
                    new XElement(EDIHelperFunctions.VendorCode, GetVendorCode()),
                    BuildContactType());
        }

        private string GetMilitaryTime()
        {
            return DateTime.Now.ToString("HH:mm");
        }
        private XElement BuildCarrier()
        {
            return new XElement(Carrier,
                new XElement(CarrierCode, new XCData(NFIL)),
             new XElement(CarrierType, CarrierTypeVaule));


        }

        private XElement BuildBOL()
        {
            int BOLNumber = GetNewBOLID();
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                StoreInfoFromEDI850 cStore = UoW.AddEDI850.Find(t => t.PONumber == m_sPO)
                                                           .Where(t => t.OrderStoreNumber == m_sCurrentStore).FirstOrDefault();

                BOLForASN cBill = new BOLForASN();
                cBill.ID = Guid.NewGuid();
                cBill.StoreInfoFK = cStore.Id;
                cBill.BOLNumber = BOLNumber;
                UoW.Bol.Add(cBill);
                int Result = UoW.Complate();

            }
            return new XElement(BillOfLadingNumber, new XCData(BOLNumber.ToString()));
        }

        private XElement BuildContactType()
        {
            ContactType cContactInfo = GetContactInfo();
            return new XElement(EDIHelperFunctions.ContactType,
                     new XElement(EDIHelperFunctions.FunctionCode, new XCData(EDIHelperFunctions.CUST)),
                     new XElement(EDIHelperFunctions.ContactName, new XCData(cContactInfo.FullName)),
                     new XElement(EDIHelperFunctions.ContactQualifier, EDIHelperFunctions.Phone),
                     new XElement(EDIHelperFunctions.PhoneEmail, new XCData(cContactInfo.PhoneNumber)),
                     new XElement(EDIHelperFunctions.ContactQualifier, EDIHelperFunctions.Email),
                     new XElement(EDIHelperFunctions.PhoneEmail, new XCData(cContactInfo.EmailAddress)));


        }


        XElement BuildShipFrom()
        {
            ShipFromInformation ShipFrom = GetShipFromAddress();
            return new XElement(NAME, new XElement(BillAndShipToCode, EDIHelperFunctions.ShipFrom),
                new XElement(EDIHelperFunctions.DUNSOrLocationNumberString, ShipFrom.DUNSOrLocationNumber),
                new XElement(EDIHelperFunctions.NameComponentQualifier, EDIHelperFunctions.DescShipFrom),
                new XElement(EDIHelperFunctions.NameComponent, new XCData(EDIHelperFunctions.ValidName)),
                new XElement(EDIHelperFunctions.CompanyName, new XCData(EDIHelperFunctions.ValidName)),
                new XElement(EDIHelperFunctions.Address, new XCData(ShipFrom.Address)),
                new XElement(EDIHelperFunctions.City, new XCData(ShipFrom.City)),
                new XElement(EDIHelperFunctions.State, ShipFrom.State),
                new XElement(EDIHelperFunctions.Zip, ShipFrom.PostalCode));

        }

        private ShipFromInformation GetShipFromAddress()
        {
            ShipFromInformation cAddreessDomain = null;

            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                cAddreessDomain = UoW.ShipFrom.Find(t => t.BillAndShipToCodes == EDIHelperFunctions.ShipFrom).FirstOrDefault();
                if (cAddreessDomain != null)
                {
                    return cAddreessDomain;
                }
                else
                {
                    throw new ExceptionsEDI(string.Format("{0} {1}", Help, ErrorCodes.HSAErro41));
                }
            }

        }

        XElement BuildShipTo()
        {
            DCInformation ShipTo = GetShipToAddress();
            return new XElement(NAME,
                    new XElement(EDIHelperFunctions.BillAndShipToCode, EDIHelperFunctions.ShipTo),
                    new XElement(EDIHelperFunctions.DUNSOrLocationNumberString, DCNumber),
                    new XElement(EDIHelperFunctions.NameComponentQualifier, EDIHelperFunctions.DescShipTo),
                    new XElement(EDIHelperFunctions.NameComponent, new XCData(string.Format("{0}    {1}", EDIHelperFunctions.SHIPPREDISTROTODC, DCNumber))),
                    new XElement(EDIHelperFunctions.CompanyName, new XCData(string.Format("{0}    {1}", EDIHelperFunctions.SHIPPREDISTROTODC, DCNumber))),
                    new XElement(EDIHelperFunctions.Address, new XCData(ShipTo.Address)),
                    new XElement(EDIHelperFunctions.City, new XCData(ShipTo.City)),
                    new XElement(EDIHelperFunctions.State, ShipTo.State),
                    new XElement(EDIHelperFunctions.Zip, ShipTo.PostalCode),
                    new XElement(EDIHelperFunctions.Country, EDIHelperFunctions.US));
        }

        XElement BuildShipTo(SpecialOrder cHoildayOrder)
        {
            DCInformation ShipTo = GetShipToAddress(cHoildayOrder);
            return new XElement(NAME,
                    new XElement(EDIHelperFunctions.BillAndShipToCode, EDIHelperFunctions.ShipTo),
                    new XElement(EDIHelperFunctions.DUNSOrLocationNumberString, cHoildayOrder.DC),
                    new XElement(EDIHelperFunctions.NameComponentQualifier, EDIHelperFunctions.DescShipTo),
                    new XElement(EDIHelperFunctions.NameComponent, new XCData(string.Format("{0}    {1}", EDIHelperFunctions.SHIPPREDISTROTODC, cHoildayOrder.DC))),
                    new XElement(EDIHelperFunctions.CompanyName, new XCData(string.Format("{0}    {1}", EDIHelperFunctions.SHIPPREDISTROTODC, cHoildayOrder.DC))),
                    new XElement(EDIHelperFunctions.Address, new XCData(ShipTo.Address)),
                    new XElement(EDIHelperFunctions.City, new XCData(ShipTo.City)),
                    new XElement(EDIHelperFunctions.State, ShipTo.State),
                    new XElement(EDIHelperFunctions.Zip, ShipTo.PostalCode),
                    new XElement(EDIHelperFunctions.Country, EDIHelperFunctions.US));

        }
        /// <summary>
        /// Build the 850 to send back to EZcom 
        /// </summary>
        public void BuildASN()
        {
            XElement File = new XElement(FILE,
                      new XElement(EDIHelperFunctions.DOCUMENT, BuildHeader(), BuildShipmentLevel(), BuildOrderLevel()));

            SaveDataToFile(File);
            SaveASN(File);
        }

        private void SaveDataToFile(XElement file)
        {

            var Setting = new XmlWriterSettings();
            Setting.Indent = true;
            Setting.NewLineOnAttributes = true;
            Setting.Encoding = Encoding.UTF8;
            Setting.WriteEndDocumentOnClose = true;
            StreamWriter cStreamWriter;

            if (m_sCompanyCode == "CER05")
            {
                cStreamWriter = File.CreateText(Path.Combine(m_settings.ASNPathAmexOldVerison, m_settings.FileASNPathAmexOldVerison));
            }
            else
            {
                cStreamWriter = File.CreateText(Path.Combine(m_settings.ASNPathVisaMcOldVerison, m_settings.FileASNPathVisaMcOldVerison));
            }
            using (XmlWriter cXmlWriter = XmlWriter.Create(cStreamWriter, Setting))
            {
                file.WriteTo(cXmlWriter);
            };

        }


        private void SaveDataToFile(XElement file, SpecialOrder Order)
        {
            var Setting = new XmlWriterSettings();
            Setting.Indent = true;
            Setting.NewLineOnAttributes = true;
            Setting.Encoding = Encoding.UTF8;
            Setting.WriteEndDocumentOnClose = true;
          //  m_sPathForASNFile = string.Format("{0}ASN Store {1} for PO {2} {3}.xml", m_sASNFolder, Order.Store, Order.PO, GetNewShipmentID);
            StreamWriter cStreamWriter = File.CreateText(m_sPathForASNFile);
            using (XmlWriter cXmlWriter = XmlWriter.Create(cStreamWriter, Setting))
            {
                file.WriteTo(cXmlWriter);
            };
        }
        /// <summary>
        /// Save ASN to a file to upload to Ezcom
        /// </summary>
        /// <param name="file"></param>
        private void SaveASN(XElement file)
        {
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                List<StoreInfoFromEDI850> lisStores = UoW.AddEDI850.Find(t => t.PONumber == m_sPO).Where(x => x.ASNStatus == (int)ASNStatus.ReadyForASN)
                                                                    .Where(X => X.OrderStoreNumber == m_sCurrentStore).ToList();
                ASNFileOutBound cASNFileOutBound = new ASNFileOutBound();
                cASNFileOutBound.Id = Guid.NewGuid();
                cASNFileOutBound.File = file.ToString();
                cASNFileOutBound.DTS = DateTime.Now;
                lisStores.ForEach(s => s.ASNStatus = (int)ASNStatus.HasASN);
                lisStores.ForEach(s => s.PickStatus = (int)EOrderStatus.Open);
                UoW.AddEDI850.SaveChange();
                UoW.ASNFile.Add(cASNFileOutBound);
                UoW.Complate();
            }
        }

        private Label AddNewLabel(XElement file)
        {
            Label cLabel = new Label();
            XmlReader reader = file.CreateReader();
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                var ShipFrom = UoW.ShipFrom.Find(t => t.BillAndShipToCodes == "SF").FirstOrDefault();
                cLabel.Faddress = ShipFrom.Address;
                cLabel.Fcity = ShipFrom.City;
                cLabel.Fstate = ShipFrom.State;
                cLabel.FZip = ShipFrom.PostalCode;
                cLabel.To = TargetStores;
                cLabel.From = ValidUSA;
                
                while (reader.Read())
                {

                    if (reader.Name == "UCC128")
                    {
                        cLabel.SSCC = reader.ReadElementContentAsObject().ToString();
                        continue;
                    }

                    if (reader.Name == "ShippingLocationNumber")
                    {
                        cLabel.OrderStoreNumber = reader.ReadElementContentAsObject().ToString();
                        continue;
                    }

                    if (reader.Name == "PurchaseOrderNumber")
                    {
                        cLabel.PONumber = reader.ReadElementContentAsObject().ToString();
                        string[] parts = cLabel.PONumber.Split('-');
                        if (parts.Length == 3)
                        {
                            string DCString = parts[2].ToString();
                            var DC = UoW.DCInfo.Find(t => t.StoreID == DCString).FirstOrDefault();
                            cLabel.DcNumber = DC.StoreID;
                            cLabel.Tcity = DC.City;
                            cLabel.Tstate = DC.State;
                            cLabel.Tzip = DC.PostalCode;
                            cLabel.Taddress = DC.Address;
                            continue;
                        }
                    }

                }


            }
            return cLabel;
        }


        /// <summary>
        /// ths is for order detail 
        /// </summary>
        /// <returns></returns>
        private XElement BuildOrderLevel(IEnumerable<SpecialOrder> Orders)
        {

            return new XElement(ORDERLEVEL,
                             new XElement(IDS,
                             new XElement(EDIHelperFunctions.PurchaseOrderNumber, new XCData(Orders.Take(1).FirstOrDefault().PO)),
                             new XElement(EDIHelperFunctions.PURCHASEORDERSOURCEID, Orders.Take(1).FirstOrDefault().DocumentId),
                             new XElement(EDIHelperFunctions.PurchaseOrderDate, Orders.Take(1).FirstOrDefault().PODate.ToString(toFormat)),
                             new XElement(EDIHelperFunctions.StoreNumber, new XCData(Orders.Take(1).FirstOrDefault().Store)),
                             new XElement(EDIHelperFunctions.DepartmentNumberString, EDIHelperFunctions.DepartmentNumber),
                             new XElement(EDIHelperFunctions.DivisionNumberString, EDIHelperFunctions.DivisionNumber),
                             new XElement(EDIHelperFunctions.ReleaseNumberString, EDIHelperFunctions.ReleaseNumber)),
                             BuildOrderTotals(Orders), BuildPickStruture(Orders));

        }
        private XElement BuildOrderLevel()
        {

            return new XElement(ORDERLEVEL,
                             new XElement(IDS,
                             new XElement(EDIHelperFunctions.PurchaseOrderNumber, new XCData(m_sPO)),
                             new XElement(EDIHelperFunctions.PURCHASEORDERSOURCEID, GetDocID),
                             new XElement(EDIHelperFunctions.PurchaseOrderDate, GetPODate),
                             new XElement(EDIHelperFunctions.StoreNumber, new XCData(m_sCurrentStore)),
                             new XElement(EDIHelperFunctions.DepartmentNumberString, EDIHelperFunctions.DepartmentNumber),
                             new XElement(EDIHelperFunctions.DivisionNumberString, EDIHelperFunctions.DivisionNumber),
                             new XElement(EDIHelperFunctions.ReleaseNumberString, EDIHelperFunctions.ReleaseNumber)),
                             BuildOrderTotals(), BuildPickStruture());

        }

        XElement BuildPickStruture(IEnumerable<SpecialOrder> Orders)
        {
            List<Carton> Cartons = BuildPickPackStructure(Orders);
            ConvertToxml(Cartons, Orders);
            return PickAndPackElement();

        }
        XElement BuildPickStruture()
        {
            List<StoreInfoFromEDI850> Orders = GetOrders();
            if (Orders.Count == 0)
            {
                throw new ExceptionsEDI(string.Format("{0} {1}", Help, ErrorCodes.HSAErro59));
            }
            List<Carton> Cartons = BuildPickPackStructure(Orders);
            ConvertToxml(Cartons);
            return PickAndPackElement();
        }
       
        XElement PickAndPackElement()
        {
            using (FileStream cFileStream = File.OpenRead(m_settings.TempPath))
            {
                XDocument doc = XDocument.Load(cFileStream);
                return new XElement(doc.Root);
            }
        }
        
        private void ConvertToxml(List<Carton> lisCarton)
        {

            var Setting = new XmlWriterSettings();
            Setting.Indent = true;
            Setting.NewLineOnAttributes = true;
            Setting.Encoding = Encoding.UTF8;
            Setting.WriteEndDocumentOnClose = true;

            StreamWriter cStreamWriter = File.CreateText(m_settings.TempPath);

            using (XmlWriter cXmlWriter = XmlWriter.Create(cStreamWriter, Setting))
            {
                cXmlWriter.WriteStartDocument();
                cXmlWriter.WriteStartElement(EDIHelperFunctions.PickPackStructure);
                foreach (Carton cCarton in lisCarton)
                {
                    cXmlWriter.WriteStartElement(EDIHelperFunctions.Carton);
                    cXmlWriter.WriteStartElement(MARKS);
                    cXmlWriter.WriteStartElement(UCC128);
                    cXmlWriter.WriteCData(cCarton.UCC128);
                    cXmlWriter.WriteEndElement(); //End of UCC128
                    cXmlWriter.WriteEndElement(); //End of Mark
                    cXmlWriter.WriteStartElement(Quantities);
                    cXmlWriter.WriteElementString(QtyQualifier, "ZZ");
                    cXmlWriter.WriteElementString(QtyUOM, "ZZ");
                    cXmlWriter.WriteElementString(Qty, cCarton.Qty.ToString());
                    cXmlWriter.WriteEndElement();
                    cXmlWriter.WriteStartElement(Measurement);
                    cXmlWriter.WriteElementString(MeasureQual, "WT");
                    cXmlWriter.WriteElementString(MeasureValue, "0.95");
                    cXmlWriter.WriteEndElement(); //Close  Measurement tag 
                    cXmlWriter.WriteEndElement(); //Close carton tag
                    
                    List<StoreInfoFromEDI850> Stores = GetStoreItmesForCartons();
                    foreach (StoreInfoFromEDI850 Store in Stores)
                    {
                        SkuItem cSkuItem = GetSkuInfo(Store);
                        cXmlWriter.WriteStartElement(ITEM);
                        cXmlWriter.WriteElementString(CustomerLineNumber, Store.CustomerLineNumber.ToString());
                        cXmlWriter.WriteStartElement(ItemIDs);
                        cXmlWriter.WriteElementString(IdQualifier, UP);
                        cXmlWriter.WriteStartElement(ID);
                        cXmlWriter.WriteCData(Store.UPCode);
                        cXmlWriter.WriteEndElement(); //close tag for upc
                        cXmlWriter.WriteEndElement(); //end of item for upc

                        cXmlWriter.WriteStartElement(ItemIDs);
                        cXmlWriter.WriteElementString(IdQualifier, VN);
                        cXmlWriter.WriteStartElement(Id);
                        cXmlWriter.WriteCData(GetCompanyCode());
                        cXmlWriter.WriteEndElement(); //close VN tag
                        cXmlWriter.WriteEndElement(); //end of item for vn 

                        cXmlWriter.WriteStartElement(ItemIDs);
                        cXmlWriter.WriteElementString(IdQualifier, CU);
                        cXmlWriter.WriteStartElement(Id);
                        cXmlWriter.WriteCData(cSkuItem.DPCI);
                        cXmlWriter.WriteEndElement(); //Close cu tag 
                        cXmlWriter.WriteEndElement(); //Close itemids 
                        cXmlWriter.WriteStartElement(Quantities);
                        cXmlWriter.WriteElementString(QtyQualifier, "39");
                        cXmlWriter.WriteElementString(QtyUOM, Each);
                        cXmlWriter.WriteElementString(Qty, Store.QtyOrdered.ToString());
                        cXmlWriter.WriteEndElement(); //Quantities

                        cXmlWriter.WriteElementString(PackSize, QtyUOMSize);
                        cXmlWriter.WriteElementString(Inners, one);
                        cXmlWriter.WriteElementString(EachesPerInner, one);
                        cXmlWriter.WriteElementString(InnersPerPacks, m_cASNSet.Packs.ToString());
                        cXmlWriter.WriteStartElement(Measurement);
                        cXmlWriter.WriteElementString(MeasureQual, "WT");
                        cXmlWriter.WriteElementString(MeasureValue, "0.038");
                        cXmlWriter.WriteEndElement(); //close tag for Measurement
                        
                        cXmlWriter.WriteStartElement(ItemDescription);
                        cXmlWriter.WriteCData(cSkuItem.Product.Trim());
                        cXmlWriter.WriteEndElement();
                        cXmlWriter.WriteEndElement();
                    }

                }
                cXmlWriter.WriteEndElement(); //End of PickPackStructure
                cXmlWriter.Close();
                cStreamWriter.Close();
            }

        }

        private void ConvertToxml(List<Carton> lisCarton, IEnumerable<SpecialOrder> Orders)
        {

            var Setting = new XmlWriterSettings();
            Setting.Indent = true;
            Setting.NewLineOnAttributes = true;
            Setting.Encoding = Encoding.UTF8;
            Setting.WriteEndDocumentOnClose = true;

            StreamWriter cStreamWriter = File.CreateText(m_settings.TempPath );

            using (XmlWriter cXmlWriter = XmlWriter.Create(cStreamWriter, Setting))
            {
                cXmlWriter.WriteStartDocument();
                cXmlWriter.WriteStartElement(EDIHelperFunctions.PickPackStructure);
                foreach (Carton cCarton in lisCarton)
                {
                    cXmlWriter.WriteStartElement(EDIHelperFunctions.Carton);
                    cXmlWriter.WriteStartElement(MARKS);
                    cXmlWriter.WriteStartElement(UCC128);
                    cXmlWriter.WriteCData(cCarton.UCC128);
                    cXmlWriter.WriteEndElement(); //End of UCC128
                    cXmlWriter.WriteEndElement(); //End of Mark
                    cXmlWriter.WriteStartElement(Quantities);
                    cXmlWriter.WriteElementString(QtyQualifier, "ZZ");
                    cXmlWriter.WriteElementString(QtyUOM, "ZZ");
                    cXmlWriter.WriteElementString(Qty, cCarton.Qty.ToString());
                    cXmlWriter.WriteEndElement();
                    cXmlWriter.WriteStartElement(Measurement);
                    cXmlWriter.WriteElementString(MeasureQual, "WT");
                    cXmlWriter.WriteElementString(MeasureValue, GetTotalWeight(Orders).ToString());
                    cXmlWriter.WriteEndElement(); //Close  Measurement tag 
                    cXmlWriter.WriteEndElement(); //Close carton tag
                    int count = 1;

                    foreach (SpecialOrder Store in Orders)
                    {
                        cXmlWriter.WriteStartElement(ITEM);
                        cXmlWriter.WriteElementString(CustomerLineNumber, count.ToString());
                        cXmlWriter.WriteStartElement(ItemIDs);
                        cXmlWriter.WriteElementString(IdQualifier, UP);
                        cXmlWriter.WriteStartElement(ID);
                        cXmlWriter.WriteCData(Store.UPC);
                        cXmlWriter.WriteEndElement(); //close tag for upc
                        cXmlWriter.WriteEndElement(); //end of item for upc

                        cXmlWriter.WriteStartElement(ItemIDs);
                        cXmlWriter.WriteElementString(IdQualifier, VN);
                        cXmlWriter.WriteStartElement(Id);
                        cXmlWriter.WriteCData(GetCompanyCode(Store));
                        cXmlWriter.WriteEndElement(); //close VN tag
                        cXmlWriter.WriteEndElement(); //end of item for vn 

                        cXmlWriter.WriteStartElement(ItemIDs);
                        cXmlWriter.WriteElementString(IdQualifier, CU);
                        cXmlWriter.WriteStartElement(Id);
                        cXmlWriter.WriteCData(Store.DPCI);
                        cXmlWriter.WriteEndElement(); //Close cu tag 
                        cXmlWriter.WriteEndElement(); //Close itemids 
                        cXmlWriter.WriteStartElement(Quantities);
                        cXmlWriter.WriteElementString(QtyQualifier, "39");
                        cXmlWriter.WriteElementString(QtyUOM, Each);
                        cXmlWriter.WriteElementString(Qty, Store.BundleCount.ToString());
                        cXmlWriter.WriteEndElement(); //Quantities
                        cXmlWriter.WriteElementString(PackSize, QtyUOMSize);
                        cXmlWriter.WriteElementString(Inners, one);
                        cXmlWriter.WriteElementString(EachesPerInner, one);
                        cXmlWriter.WriteElementString(InnersPerPacks, m_cASNSet.Packs.ToString());
                        cXmlWriter.WriteStartElement(Measurement);
                        cXmlWriter.WriteElementString(MeasureQual, "WT");
                        cXmlWriter.WriteElementString(MeasureValue, "0.038");
                        cXmlWriter.WriteEndElement(); //close tag for Measurement

                        cXmlWriter.WriteStartElement(ItemDescription);
                        cXmlWriter.WriteCData(Store.ItemDescription.Trim());
                        cXmlWriter.WriteEndElement();
                        cXmlWriter.WriteEndElement();
                        ++count;
                    }

                }
                cXmlWriter.WriteEndElement(); //End of PickPackStructure
                cXmlWriter.Close();
                cStreamWriter.Close();
            }

        }

        private string GetCompanyCode()
        {
            if (!string.IsNullOrEmpty(m_sCompanyCode))
            {
                if (m_sCompanyCode == "CER05")
                {
                    return ASNVNSettingAMEX;
                }
                else
                {
                    return ASNVNSettingMC;
                }
            }
            return string.Empty;
        }

        private string GetCompanyCode(SpecialOrder Order)
        {
            if (Order.CompanyCode == "CER05")
            {
                return ASNVNSettingAMEX;
            }
            else
            {
                return ASNVNSettingMC;
            }
            
        }


        private SkuItem GetSkuInfo(StoreInfoFromEDI850 store)
        {
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                return UoW.Sku.Find(t => t.Id == store.SkuItemFK).FirstOrDefault();
            }
        }
        
        private List<StoreInfoFromEDI850> GetStoreItmesForCartons()
        {
            List<StoreInfoFromEDI850> Stores = new List<StoreInfoFromEDI850>();
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                return UoW.AddEDI850.Find(t => t.PONumber == m_sPO).Where(x => x.ASNStatus == (int)ASNStatus.ReadyForASN)
                                                                .Where(X => X.OrderStoreNumber == m_sCurrentStore).ToList();
            }

        }
        
        /// <summary>
        /// Get all the stores for a given carton 
        /// </summary>
        /// <param name="Test"></param>
        /// <param name="StoreFK"></param>
        /// <returns></returns>
        private IEnumerable<StoreInfoFromEDI850> GetStoreItmesForCartons(bool Test, Guid StoreFK)
        {
            List<StoreInfoFromEDI850> Stores = new List<StoreInfoFromEDI850>();
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                var temp = UoW._Cartons.Find(t => t.StoreNumberFK == StoreFK).ToList();
                foreach (var item in temp)
                {
                    Stores.Add(UoW.AddEDI850.Find(t => t.Id == item.Id).FirstOrDefault());
                }
            }

            return Stores;
        }
        

        /// <summary>
        /// Fill carton with sku items
        /// </summary>
        /// <param name="lisStore"></param>
        /// <returns></returns>
        private List<Carton> BuildPickPackStructure(List<StoreInfoFromEDI850> lisStore)
        {
            return FillBox(lisStore, m_cASNSet.MaxWeightFullBoxes);
        }

        private List<Carton> BuildPickPackStructure(IEnumerable<SpecialOrder> lisStore)
        {
            return FillBox(lisStore, GetTotalQTY(lisStore));
        }


        private List<Carton> FillBox(IEnumerable<SpecialOrder> cHoildayOrders, int Qty)
        {
            List<Carton> lisCarton = new List<Domain.Carton>();
            Carton cCarton = new Domain.Carton();
            cCarton = GetSSCCForNewOrder();
            cCarton.Qty = Qty;
            cCarton.Weight = 1;
            lisCarton.Add(cCarton);
            return lisCarton;
        }


        /// <summary>
        /// Fill cartons based on the number of order items 
        /// </summary>
        /// <param name="lisStore"></param>
        /// <param name="MaxWeght"></param>
        /// <returns></returns>
        private List<Carton> FillBox(List<StoreInfoFromEDI850> lisStore, int MaxWeght)
        {
            List<Carton> lisCarton = new List<Domain.Carton>();
            Carton cCarton = new Domain.Carton();
            bool bNewCarton = false;
            int CustomerLineNumber = 1;
            cCarton = GetSSCCForNewOrder();
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                foreach (StoreInfoFromEDI850 cStore in lisStore)
                {
                    var temp = UoW.AddEDI850.Find(t => t.Id == cStore.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        int OrderWeight = (temp.QtyOrdered / m_cASNSet.Packs);
                        int CurrentWeight = 0;
                        SkuItem ItemDescription = GetItemDescription(temp.UPCode);
                        if (OrderWeight <= MaxWeght)
                        {
                            cCarton.Qty += temp.QtyOrdered;
                            CurrentWeight = OrderWeight;
                        }
                        else
                        {
                            cCarton.Qty = MaxWeght;
                            CurrentWeight = MaxWeght;
                        }

                        while (OrderWeight > 0) //Keep adding items to box untill QtyOrdered = 0
                        {
                            if (bNewCarton)
                            {
                                cCarton.StoreNumberFK = temp.Id;
                                cCarton.StoreNumber = temp;
                                temp.Carton.Add(cCarton);
                                lisCarton.Add(cCarton);
                                cCarton = GetSSCCForNewOrder();
                                cCarton.Qty += CurrentWeight;
                                bNewCarton = false;
                                CustomerLineNumber = 1;
                            }
                            cCarton.Weight += CurrentWeight;
                            temp.CustomerLineNumber = CustomerLineNumber;
                            temp.QtyPacked = 0;

                            CustomerLineNumber++;
                            if (OrderWeight >= MaxWeght)
                            {
                                OrderWeight -= MaxWeght;
                                bNewCarton = true;
                                CurrentWeight = OrderWeight;
                            }
                            else
                            {
                                OrderWeight -= CurrentWeight;

                            }

                        }
                        cCarton.StoreNumberFK = temp.Id;
                        cCarton.StoreNumber = temp;

                        temp.Carton.Add(cCarton);
                        int Result = UoW.AddEDI850.SaveChange();
                    }


                }
            }

            lisCarton.Add(cCarton);
            return lisCarton;
        }


        private SkuItem GetItemDescription(string uPCode)
        {
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                return UoW.Sku.Find(t => t.ProductUPC == uPCode.Replace(@"'", "")).FirstOrDefault();
            }
        }

        XElement BuildOrderTotals()
        {
            return new XElement(ORDERTOTALS,
                    new XElement(OrderTotalCases, GetShippingWeightOrBoxCount(ShippingInfoType.BoxCount)),
                     new XElement(OrderTotalWeight, GetShippingWeightOrBoxCount(ShippingInfoType.ShippingWeight)));

        }
        XElement BuildOrderTotals(IEnumerable<SpecialOrder> Orders)
        {
            return new XElement(ORDERTOTALS,
                     new XElement(OrderTotalCases, 1),
                      new XElement(OrderTotalWeight, GetTotalWeight(Orders)));
        }

        private double GetTotalWeight(IEnumerable<SpecialOrder> orders)
        {
            return (orders.Sum(t => t.QtyOrdered) / 25) * EDIHelperFunctions.AmexWeight;
        }

        private int GetTotalQTY(IEnumerable<SpecialOrder> orders)
        {
            return (orders.Sum(t => t.QtyOrdered) / 25);

        }

        XElement BuildOrderTotals(SpecialOrder cHoildayOrder)
        {
            return new XElement(ORDERTOTALS,
                  new XElement(OrderTotalCases, GetNumberofCases(cHoildayOrder)),
                   new XElement(OrderTotalWeight, GetMaxWeight(cHoildayOrder)));

        }

        private int GetMaxWeight(SpecialOrder cHoildayOrder)
        {
            int QtyOrdered = cHoildayOrder.BundleCount;
            return 0;
        }

        private int GetNumberofCases(SpecialOrder cHoildayOrder)
        {
            return cHoildayOrder.BundleCount / cHoildayOrder.NumberofPacksPerBox;
        }

        #region Class functions 
        private ContactType GetContactInfo()
        {
            ContactType cContactInfo = null;
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                cContactInfo = UoW.ASNContact.GetAll().FirstOrDefault();
                if (cContactInfo != null)
                {
                    return cContactInfo;
                }
                else
                {
                    throw new ExceptionsEDI(string.Format("{0}{1}{2}", EDIHelperFunctions.Help, EDIHelperFunctions.Space, ErrorCodes.HSAErro19));
                }
            }

        }

        private string GetVendorCode()
        {
            string sVendorCode = string.Empty;
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                StoreInfoFromEDI850 _Store = UoW.AddEDI850.Find(t => t.OrderStoreNumber == m_sCurrentStore).FirstOrDefault();

                if (!string.IsNullOrEmpty(_Store.VendorNumber))
                {
                    return _Store.VendorNumber;
                }
                else
                {
                    throw new ExceptionsEDI(string.Format("{0} {1}", EDIHelperFunctions.Help, ErrorCodes.HSAErro17));
                }
            }
        }

        private string DCNumber
        {
            get
            {
                string sStoreNumber = string.Empty;
                using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
                {
                    StoreInfoFromEDI850 lisInbound850 = UoW.AddEDI850.Find(t => t.PONumber == m_sPO).FirstOrDefault();
                    if (!string.IsNullOrEmpty(lisInbound850.DCNumber))
                    {
                        sStoreNumber = lisInbound850.DCNumber.Replace(@"'", "");
                    }
                    else
                    {
                        throw new ExceptionsEDI(string.Format("{0} {1}", EDIHelperFunctions.Help, ErrorCodes.HSAErro27));
                    }
                }

                return sStoreNumber;

            }

        }

        private DCInformation GetShipToAddress(SpecialOrder cHoildayOrder)
        {
            DCInformation cDCInformation = null;

            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                cDCInformation = UoW.DCInfo.Find(t => t.StoreID == cHoildayOrder.DC).FirstOrDefault();

                if (cDCInformation != null)
                {
                    return cDCInformation;
                }
                else
                {
                    throw new ExceptionsEDI(string.Format("{0} {1}", EDIHelperFunctions.Help, ErrorCodes.HSAErro41));
                }
            }
        }

        private DCInformation GetShipToAddress()
        {
            DCInformation cDCInformation = null;

            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                //DCNumber.Replace(@"'", "")
                string DCNumber = UoW.AddEDI850.Find(x => x.PONumber == m_sPO).Where(x => x.OrderStoreNumber == m_sCurrentStore)
                                                        .Where(x => x.ASNStatus == (int)ASNStatus.ReadyForASN).FirstOrDefault().DCNumber;

                string sTempStore = m_sCurrentDCNumber.Replace(@"'", "");

                cDCInformation = UoW.DCInfo.Find(t => t.StoreID == sTempStore).FirstOrDefault();

                if (cDCInformation != null)
                {

                    return cDCInformation;
                }
                else
                {
                    throw new ExceptionsEDI(string.Format("{0} {1}", EDIHelperFunctions.Help, ErrorCodes.HSAErro41));
                }
            }
        }

        private string GetPODate
        {
            get
            {
                using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
                {
                    StoreInfoFromEDI850 cInbound850 = UoW.AddEDI850.Find(t => t.PONumber == m_sPO)
                                                     .Where(x => x.OrderStoreNumber == m_sCurrentStore)
                                                     .Where(x => x.ASNStatus == (int)ASNStatus.ReadyForASN).FirstOrDefault();
                    if (cInbound850 != null)
                    {
                        return cInbound850.PODate.ToString(toFormat);
                    }
                    else
                    {
                        throw new ExceptionsEDI(string.Format("{0} {1}", EDIHelperFunctions.Help, ErrorCodes.HSAErro45));
                    }
                }
            }
        }

        private string GetDocID
        {
            get
            {
                using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
                {
                    StoreInfoFromEDI850 store = UoW.AddEDI850.Find(t => t.PONumber == m_sPO)
                                               .Where(t => t.ASNStatus == (int)ASNStatus.ReadyForASN)
                                               .Where(t => t.OrderStoreNumber == m_sCurrentStore).FirstOrDefault();
                    if (!string.IsNullOrEmpty(store.DocumentId))
                    {
                        return store.DocumentId;
                    }
                    else
                    {
                        throw new ExceptionsEDI(string.Format("{0} {1}", EDIHelperFunctions.Help, ErrorCodes.HSAErro45));
                    }
                }
            }
        }

        private Carton GetSSCCForNewOrder()
        {
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                return new Carton() { Id = Guid.NewGuid(), UCC128 = UoW.SSCCBarcode.GetNextSequenceNumber(SSCCStatus.NotUsed) };
            }
        }



        private List<Carton> GetSSCCForNewOrder(int boxTotal)
        {
            List<Carton> lisNewSSCC = new List<Carton>();
            if (boxTotal < 1)
            {
                return null;
            }
            for (int i = 0; i < boxTotal; i++)
            {
                using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
                {
                    string sBarCode = string.Empty;
                    sBarCode = UoW.SSCCBarcode.GetNextSequenceNumber(SSCCStatus.NotUsed);
                    Carton cNewSSCC = new Carton() { Id = Guid.NewGuid(), UCC128 = sBarCode };
                    lisNewSSCC.Add(cNewSSCC);
                }
            }
            return lisNewSSCC;
        }

        private List<StoreInfoFromEDI850> GetOrders()
        {
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                return UoW.AddEDI850.Find(t => t.PONumber == m_sPO).Where(x => x.ASNStatus == (int)ASNStatus.ReadyForASN)
                                                                   .Where(X => X.OrderStoreNumber == m_sCurrentStore).ToList();
            }
        }

        public double GetShippingWeightOrBoxCount(ShippingInfoType ShippingInfo)
        {

            double dTotalWeight = 0;
            double BundleWeight = 0;
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                List<StoreInfoFromEDI850> Stores = UoW.AddEDI850.Find(t => t.PONumber == m_sPO)
                              .Where(x => x.OrderStoreNumber == m_sCurrentStore)
                              .Distinct()
                              .ToList();


                BundleWeight = Stores.FirstOrDefault().PkgWeight;
                foreach (StoreInfoFromEDI850 item in Stores)
                {
                    dTotalWeight += item.QtyOrdered;

                }

                double SubTotal = dTotalWeight / m_cASNSet.Packs;
                //SubTotal *= BundleWeight;

                switch (ShippingInfo)
                {
                    case ShippingInfoType.PickCount:

                        return (SubTotal);


                    case ShippingInfoType.ShippingWeight:

                        return SubTotal + m_cASNSet.EmptyBoxWeights;


                    case ShippingInfoType.BoxCount:

                        return GetNumberofBoxes(SubTotal);

                    default:
                        return 0;

                }

            }



        }

        private int GetNumberofBoxes(double packs)
        {

            int iCount = 0;
            if (packs == 0)
            {
                return 0;
            }
            if (packs <= m_cASNSet.MinWeightForShippings || packs <= (m_cASNSet.MaxWeightFullBoxes - 1))
            {
                iCount++;
                return iCount;

            }
            else
            {

                while (packs >= (m_cASNSet.MaxWeightFullBoxes - 1))
                {
                    packs = packs - m_cASNSet.MaxWeightFullBoxes;
                    iCount++;
                }
                if (packs >= 1)
                {
                    iCount++;
                }
                return iCount;

            }
        }

        /// <summary>
        /// Make new shipment ID
        /// </summary>
        private string GetNewShipmentID
        {
            get
            {
                return EDIHelperFunctions.VPS + EDIHelperFunctions.RandomString(EDIHelperFunctions.shiplength);
            }
        }

        private int GetNewBOLID()
        {
            Random cRandom = new Random();
            return cRandom.Next(100000, 100000000);
        }

        private string GetCustomerNumber()
        {
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld)))
            {

                StoreInfoFromEDI850 cEDI850 = UoW.AddEDI850.Find(t => t.PONumber == m_sPO)
                                             .Where(t => t.ASNStatus == (int)ASNStatus.ReadyForASN)
                                             .Where(t => t.OrderStoreNumber == m_sCurrentStore)
                                             .FirstOrDefault();
                if (cEDI850 != null)
                {
                    return cEDI850.CustomerNumber;
                }
                throw new ExceptionsEDI(string.Format("{0} {1}", Help, ErrorCodes.HSAErro47));
            }
        }

        /// <summary>
        /// Set the code for AMEX or Visa 
        /// </summary>
        public void SetCompanyCode()
        {
            using (var UoW = new OldUnitofWork(new OldEDIContext(m_settings.ConnectionStringOld )))
            {
                StoreInfoFromEDI850 cEDI850 = UoW.AddEDI850.Find(t => t.PONumber == m_sPO)
                                             .Where(t => t.ASNStatus == (int)ASNStatus.ReadyForASN)
                                             .Where(t => t.OrderStoreNumber == m_sCurrentStore)
                                             .FirstOrDefault();
                if (cEDI850 != null)
                {
                    m_sCompanyCode = cEDI850.CompanyCode;
                }
                else
                {
                    throw new ExceptionsEDI(string.Format("{0} {1}", Help, ErrorCodes.HSAErro13));
                }
            }
        }
        #endregion


    }
}
